package de.tenvanien.tenuschKalender.main;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.model.calendar.NotificationScheduler;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ThemeHandler;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.systemTray.Notification;
import de.tenvanien.tenuschKalender.model.systemTray.TenuschSystemTray;
import de.tenvanien.tenuschKalender.view.BenutzerLoginGUI;
import de.tenvanien.tenuschKalender.view.DatabaseLoginGUI;
import de.tenvanien.tenuschKalender.view.KalenderGUI;

public class TenuschKalender {

	private static KalenderGUI kalender;
	private static DatabaseLoginGUI databaseLogin;
	private static BenutzerLoginGUI accountLogin;
	private static TenuschSystemTray tray;
	private static NotificationScheduler timer;

	public static void main(String[] args) {

		try {
			ThemeHandler.saveDefaultThemes();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(0);
		}

		ThemeHandler theme = new ThemeHandler("gentle_dark");
		try {
			theme.loadThemes();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			UIManager.setLookAndFeel(new Win11LookAndFeel(Win11LookAndFeel.themeName));
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		boolean connectionWorks = false;
		if (DataConnectionHandler.getInstance().getServerName() != null) {
			try {
				DatabaseHandler.getInstance().setDataSource(DataConnectionHandler.getInstance().getPool());
				DatabaseHandler.getInstance().testConnection();
				DatabaseHandler.getInstance().saveDefaultTables();
				connectionWorks = true;
			} catch (SQLException e) {}
		}

		boolean userLogingWorks = false;
		if (DataConnectionHandler.getInstance().getUsername() != null && DataConnectionHandler.getInstance().getPassword() != null) {
			try {
				ResultSet set = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(benutzername) AS userExisting FROM t_benutzer WHERE benutzername=\"" + DataConnectionHandler.getInstance().getUsername() + "\" AND passwort=\"" + DataConnectionHandler.getInstance().getPassword().hashCode() + "\";", true);
				set.next();
				int usernumber = set.getInt("userExisting");
				if (usernumber != 0) {
					userLogingWorks = true;
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		kalender = new KalenderGUI();
		timer = new NotificationScheduler();
		accountLogin = new BenutzerLoginGUI();

		if (!connectionWorks) {
			databaseLogin = new DatabaseLoginGUI();
			databaseLogin.setVisible(true);
		} else {
			if (!userLogingWorks) accountLogin.setVisible(true);
			else {
				TenuschKalender.getKalenderGUI().setVisible(true);
				try {
					Notification.loadNotifications();
				} catch (IOException e) {
					e.printStackTrace();
				}
				timer.startTimer();

				TenuschKalender.createTenuschSystemTray();
			}
		}
	}

	public static KalenderGUI getKalenderGUI() {
		return kalender;
	}

	public static void setKalenderGUI(KalenderGUI kalendernew) {
		kalender = kalendernew;
	}

	public static DatabaseLoginGUI getDatabaseLoginGUI() {
		return databaseLogin;
	}

	public static BenutzerLoginGUI getBenutzerLoginGUI() {
		return accountLogin;
	}

	public static void createTenuschSystemTray() {
		tray = new TenuschSystemTray();
	}

	public static TenuschSystemTray getTenuschSystemTray() {
		return tray;
	}

	public static NotificationScheduler getTimer() {
		return timer;
	}
	
}
