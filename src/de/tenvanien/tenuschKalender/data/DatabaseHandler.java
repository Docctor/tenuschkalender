/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.mariadb.jdbc.MariaDbPoolDataSource;

public class DatabaseHandler {
	
	private static DatabaseHandler handler;
	
	private MariaDbPoolDataSource dataSource;
	private Connection connection;
	
	//CONSTRUCTOR
	private DatabaseHandler() throws SQLException {
		dataSource = new MariaDbPoolDataSource();
	}
	//END OF CONSTRUCTOR
	
	public static DatabaseHandler getInstance() throws SQLException {
		if (handler == null) handler = new DatabaseHandler();
		return handler;
	}
	//END OF SINGLETON
	
	//MORE METHODS
	/**
	 * Checks if the connection is valid
	 * @throws SQLException
	 */
	public void testConnection() throws SQLException {
		try (Connection conn = dataSource.getConnection()) {
	        if (!conn.isValid(1000)) {
	            throw new SQLException("Could not establish database connection.");
	        }
	    }
	}
	
	/**
	 * Sends a sql command to the database
	 * @param sqlCommand
	 * @param isRequest true if the command is a request; false if the command don't need a ResulSet
	 * @return The ResultSet
	 * @throws SQLException
	 */
	public ResultSet executeCommand(String sqlCommand, boolean isRequest) throws SQLException {
		if (connection == null) connection = dataSource.getConnection();
		if (!connection.isValid(0)) connection = dataSource.getConnection();
		
		Statement stmt = connection.createStatement();
		if (isRequest) {
			
			return stmt.executeQuery(sqlCommand);
			
		} else {
			
			stmt.execute(sqlCommand);
			return null;
			
		}
	}
	
	public void reconnect() throws SQLException {
		connection = dataSource.getConnection();
	}

	/**
	 * If the tables in the Database don't exist they will be created
	 * @throws SQLException
	 */
	public void saveDefaultTables() throws SQLException {
		executeCommand(
				  "CREATE TABLE IF NOT EXISTS t_Benutzer ("
				+ "P_UniqueId CHAR(36), "
				+ "benutzername VARCHAR(20), "
				+ "passwort VARCHAR(50), "
				+ "CONSTRAINT PK_Benutzer PRIMARY KEY (P_UniqueId)"
				+ ");"
				, false);
		executeCommand(
				  "CREATE TABLE IF NOT EXISTS t_ereignis ("
		        + "P_UniqueId CHAR(36), "
				+ "F_Benutzer CHAR(36), "
				+ "titel VARCHAR(40), "
				+ "farbe VARCHAR(13), "
				+ "beschreibung VARCHAR(128), "
				+ "startzeit BIGINT, "
				+ "endzeit BIGINT, "
//				+ "wiederholung VARCHAR(5), "
				+ "CONSTRAINT PK_Ereignis PRIMARY KEY (P_UniqueId), "
				+ "CONSTRAINT FK_Benutzer FOREIGN KEY (F_Benutzer) REFERENCES t_benutzer(P_UniqueId)"
				+ ");"
				, false);
//		executeCommand(
//				  "CREATE TABLE IF NOT EXISTS t_erinnerung ("
//				+ "P_UniqueId CHAR(36), "
//				+ "F_Ereignis CHAR(36), "
//				+ "zeit VARCHAR(30), "
//				+ "isNoted BOOL, "
//				+ "CONSTRAINT PK_Erinnerung PRIMARY KEY (P_UniqueId), "
//				+ "CONSTRAINT FK_Ereignis FOREIGN KEY (F_Ereignis) REFERENCES t_ereignis(P_UniqueId)"
//				+ ");"
//				, false);
	}

	public MariaDbPoolDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(MariaDbPoolDataSource dataSource) throws SQLException {
		this.dataSource = dataSource;
		dataSource.setLoginTimeout(5);
	}

}
