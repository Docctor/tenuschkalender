package de.tenvanien.tenuschKalender.model.systemTray;

import java.awt.TrayIcon.MessageType;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.calendar.Event;
import de.tenvanien.tenuschKalender.view.NotificationGUI;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;

public class Notification {

	String caption, message;
	MessageType windowsMessageType;
	int tenuschMessageType;

	private static Properties prop;
	private static File file = new File("./notifications.properties");

	public Notification(String caption, String message, MessageType windowsMessageType) {
		this.caption = caption;
		this.message = message;
		this.windowsMessageType = windowsMessageType;
	}

	public Notification(String caption, String message, int tenuschMessageType) {
		this.caption = caption;
		this.message = message;
		this.tenuschMessageType = tenuschMessageType;
	}

	public void displayTrayNotification(){
		TenuschKalender.getTenuschSystemTray().getTrayIcon().displayMessage(caption, message, windowsMessageType);
	}

	public void displayTenuschNotification() {
		new NotificationGUI(caption, message, tenuschMessageType);
	}

	public static void loadNotifications() throws IOException {
		prop = new Properties();
		if (file.exists()) {
			Reader reader = new FileReader(file);
			prop.load(reader);

			try {
				
				ResultSet userUUID = DatabaseHandler.getInstance().executeCommand("SELECT P_UniqueId AS userUUID FROM t_benutzer WHERE benutzername=\"" + DataConnectionHandler.getInstance().getUsername() + "\";", true);
                userUUID.next();

                String benutzerUUID = userUUID.getString("userUUID");
			
			
				String[] keys = Arrays.copyOf(prop.keySet().toArray(), prop.keySet().toArray().length, String[].class);
				for (String key : keys) {
//					System.out.println(key);
					ResultSet ereignisse = DatabaseHandler.getInstance().executeCommand("SELECT startzeit FROM t_ereignis WHERE F_Benutzer=\"" + benutzerUUID + "\" AND P_UniqueId=\"" + key + "\";", true);
					ereignisse.next();
					long time = ereignisse.getLong("startzeit");
					String raw = prop.getProperty(key);
					String[] notifyRaw = raw.split(",");
	
					String[] notifications = notifyRaw[0].split(";");
					String[] notifyBools = notifyRaw[1].split(";");
	
					UUID uuid = UUID.fromString(key);
	
					Event event = new Event(uuid, notifications, notifyBools);
					event.setStart(time);
					Event.events.put(uuid.toString(), event);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void addNotification(Event event) {
		String notificationString = "";
		String[] notifications = event.getNotifications();
		for (String string : notifications) {
			notificationString = notificationString + string + ";";
		}
		notificationString = notificationString.substring(0, notificationString.length()-1);

		String notifyBoolString = "";
		boolean[] notifyBools = event.getNotiIsNoted();
		for (boolean notifyBool : notifyBools) {
			notifyBoolString = notifyBoolString + notifyBool + ";";
		}
		notifyBoolString = notifyBoolString.substring(0, notifyBoolString.length()-1);

		String output = notificationString + "," + notifyBoolString;

		String uuid = event.getUniqueId().toString();

		prop.setProperty(uuid, output);
	}

	public static void saveNotifications() throws IOException {
		Writer writer = new FileWriter(file);
		prop.store(writer, "");
	}

	public static void saveAllNotifications() throws IOException {

		String keys[] = Arrays.copyOf(Event.events.keySet().toArray(), Event.events.keySet().toArray().length, String[].class);
		prop = new Properties();

		for (String string1 : keys) {
			Event event = Event.events.get(string1);
			String notificationString = "";
			String[] notifications = event.getNotifications();
			for (String string : notifications) {
				notificationString = notificationString + string + ";";
			}
			notificationString = notificationString.substring(0, notificationString.length()-1);

			String notifyBoolString = "";
			boolean[] notifyBools = event.getNotiIsNoted();
			for (boolean notifyBool : notifyBools) {
				notifyBoolString = notifyBoolString + notifyBool + ";";
			}
			notifyBoolString = notifyBoolString.substring(0, notifyBoolString.length()-1);

			String output = notificationString + "," + notifyBoolString;

			String uuid = event.getUniqueId().toString();

			prop.setProperty(uuid, output);
		}

		saveNotifications();
	}
}
