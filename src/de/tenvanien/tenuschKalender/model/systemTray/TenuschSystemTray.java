package de.tenvanien.tenuschKalender.model.systemTray;

import java.awt.AWTException;
import java.awt.Font;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.view.EinstellungsGUI;
import de.tenvanien.tenuschKalender.view.EreignisGUI;

public class TenuschSystemTray {

	TrayIcon trayIcon;

	public TenuschSystemTray(){
		SystemTray tray = SystemTray.getSystemTray();

		PopupMenu popup = new PopupMenu();
		Font popupFont = new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getStyle(), 12);

		MenuItem header = new MenuItem("Tenusch Kalender");
		header.setEnabled(false);
		popup.add(header);
		
		popup.addSeparator();
		
		MenuItem oeffnen = new MenuItem("�ffnen");
		oeffnen.setFont(popupFont);
		oeffnen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (TenuschKalender.getKalenderGUI().isVisible()) {
					TenuschKalender.getKalenderGUI().setVisible(false);
				} else {
					TenuschKalender.getKalenderGUI().setVisible(true);
				}
			}
		});
		popup.add(oeffnen);

		MenuItem add = new MenuItem("Ereignis hinzuf�gen");
		add.setFont(popupFont);
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EreignisGUI gui = new EreignisGUI(System.currentTimeMillis());
				gui.setVisible(true);
			}
		});
		popup.add(add);

		popup.addSeparator();

		MenuItem settings = new MenuItem("Einstellungen");
		settings.setFont(popupFont);
		settings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EinstellungsGUI gui = new EinstellungsGUI();
				gui.setVisible(true);
			}
		});
		popup.add(settings);

//		MenuItem synchro = new MenuItem("Synchronisieren");
//		synchro.setFont(popupFont);
//		synchro.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//
//			}
//		});
//		popup.add(synchro);

		popup.addSeparator();

		MenuItem exit = new MenuItem("Beenden");
		exit.setFont(popupFont);
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		popup.add(exit);


		Image image = Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon();
		trayIcon = new TrayIcon(image, "TenuschKalender", popup);
		trayIcon.setImageAutoSize(true);


		trayIcon.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (TenuschKalender.getKalenderGUI().isVisible()) {
						TenuschKalender.getKalenderGUI().setVisible(false);
					} else {
						TenuschKalender.getKalenderGUI().setVisible(true);
					}
				}
			}
		});

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public TrayIcon getTrayIcon() {
		return trayIcon;
	}
}
