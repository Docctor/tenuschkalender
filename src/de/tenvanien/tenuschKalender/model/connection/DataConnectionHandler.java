/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.connection;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbPoolDataSource;

import de.tenvanien.tenuschKalender.model.cryption.XORCryption;
import de.tenvanien.tenuschKalender.model.cryption.XORDecryption;
import de.tenvanien.tenuschKalender.model.cryption.XOREncryption;
import de.tenvanien.tenuschKalender.model.fileIOSystem.IOMode;
import de.tenvanien.tenuschKalender.model.fileIOSystem.IOStream;
import de.tenvanien.tenuschKalender.model.fileIOSystem.UnkownIOMode;
import de.tenvanien.tenuschKalender.model.fileIOSystem.WrongIOMode;

public class DataConnectionHandler {
	
	private String username;
	private String password;
	
	private String serverName;
	private int port = -1;
	private String database;
	private String databaseUser;
	private String databasePassword;
	
	private File databaseFile = new File("./database.dat");
	private File databaseKeyFile = new File("./databaseKey.dat");
	
	private File userFile = new File("./user.dat");
	private File userKeyFile = new File("./userKey.dat");
	
	private static DataConnectionHandler dataConnectionHandler;
	
	//CONSTRUCTOR
	private DataConnectionHandler() {
		
		if (databaseFile.exists() && databaseKeyFile.exists()) {
			try {
				IOStream databaseStream = new IOStream(databaseFile, IOMode.READ);
				String message = "";
				String line = "";
				if ((line = databaseStream.readNextLine()) != null) {
					message = message + line;
				}
				databaseStream.close();
				
				IOStream databaseKeyStream = new IOStream(databaseKeyFile, IOMode.READ);
				String key = "";
				line = "";
				if ((line = databaseKeyStream.readNextLine()) != null) {
					key = key + line;
				}
				databaseKeyStream.close();
				
				if (!message.equals("")) {
					String cryptedMessage = message;
					
					XORDecryption decryption = new XORDecryption(cryptedMessage, key);
					
					String databaseString = XORCryption.convertBinaryToString(decryption.getDecryptedBinaryMessage());
					String[] databaseInfos = databaseString.split(";");
					serverName = databaseInfos[0];
					port = Integer.parseInt(databaseInfos[1]);
					database = databaseInfos[2];
					databaseUser = databaseInfos[3];
					databasePassword = (databaseInfos[4].equals(" ") ? "" : databaseInfos[4]);
				}
				
				
			} catch (IOException | UnkownIOMode | WrongIOMode e) {
				e.printStackTrace();
			}
		}
		
		if (userFile.exists() && userKeyFile.exists()) {
			try {
				IOStream userStream = new IOStream(userFile, IOMode.READ);
				String message = "";
				String line = "";
				if ((line = userStream.readNextLine()) != null) {
					message = message + line;
				}
				userStream.close();
				
				IOStream userKeyStream = new IOStream(userKeyFile, IOMode.READ);
				String key = "";
				line = "";
				if ((line = userKeyStream.readNextLine()) != null) {
					key = key + line;
				}
				userKeyStream.close();
				
				if (!message.equals("")) {
					String cryptedMessage = message;
					
					XORDecryption decryption = new XORDecryption(cryptedMessage, key);
					
					String databaseString = XORCryption.convertBinaryToString(decryption.getDecryptedBinaryMessage());
					String[] databaseInfos = databaseString.split(";");
					username = databaseInfos[0];
					password = databaseInfos[1];
				}
			} catch (IOException | UnkownIOMode | WrongIOMode e) {
				e.printStackTrace();
			}
		}
		
	}

	public static DataConnectionHandler getInstance(){
		if (dataConnectionHandler == null) dataConnectionHandler = new DataConnectionHandler();
		return dataConnectionHandler;
	}
	
	public boolean saveUserConnection() throws IOException {
		String connection = username + ";" + password;
		String binaryConnection = XORCryption.convertStringToBinary(connection);
		XOREncryption encryption = new XOREncryption(binaryConnection);
		
		String cryptedMessage = encryption.getEncryptedBinaryMessage();
		String key = encryption.getKey();
		
		try {
			IOStream streamDatabase = new IOStream(userFile, IOMode.WRITE);
			streamDatabase.addLine(cryptedMessage);
			streamDatabase.flush();
			streamDatabase.close();
			
			IOStream streamKey = new IOStream(userKeyFile, IOMode.WRITE);
			streamKey.addLine(key);
			streamKey.flush();
			streamKey.close();
			return true;
		} catch (UnkownIOMode | WrongIOMode e) {
			return false;
		}
	}
	
	public boolean saveDatabaseConnection() throws IOException {
		String thePassword = databasePassword;
		if (thePassword.equals("")) thePassword = " ";
		String connection = serverName + ";" + port + ";" + database + ";" + databaseUser + ";" + thePassword;
		String binaryConnection = XORCryption.convertStringToBinary(connection);
		XOREncryption encryption = new XOREncryption(binaryConnection);
		
		String cryptedMessage = encryption.getEncryptedBinaryMessage();
		String key = encryption.getKey();
		
		try {
			IOStream streamDatabase = new IOStream(databaseFile, IOMode.WRITE);
			streamDatabase.addLine(cryptedMessage);
			streamDatabase.flush();
			streamDatabase.close();
			
			IOStream streamKey = new IOStream(databaseKeyFile, IOMode.WRITE);
			streamKey.addLine(key);
			streamKey.flush();
			streamKey.close();
			return true;
		} catch (UnkownIOMode | WrongIOMode e) {
			return false;
		}
	}
	
	public boolean deleteDatabaseConnection() {
		return databaseFile.delete() && databaseKeyFile.delete();
	}
	
	public boolean deleteUserConnection() {
		return userFile.delete() && userKeyFile.delete();
	}
	
	//GETTERS AND SETTERS
	
	public MariaDbPoolDataSource getPool() throws SQLException {
		MariaDbPoolDataSource source = new MariaDbPoolDataSource(serverName, port, database);
		source.setPassword(databasePassword);
		source.setUser(databaseUser);
		return source;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getDatabaseUser() {
		return databaseUser;
	}

	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}
}
