/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.cryption;

public class XORDecryption {

	private String cryptedMessage;
	private String message;
	
	private String key;
	
	public XORDecryption(String message, String key) {
		this.key = key;
		this.cryptedMessage = message;
	}
	
	public String getDecryptedBinaryMessage() {
		if (message != null) return message;
		
		message = "";
		int keyIndex = 0;
		for (int i = 0; i < cryptedMessage.length(); i++) {
			int messageNumber = Integer.parseInt(String.valueOf(cryptedMessage.charAt(i)));
			int keyNumber = Integer.parseInt(String.valueOf(key.charAt(keyIndex)));
			if (messageNumber == keyNumber) message = message + "0";
			else message = message + "1";
			
			keyIndex++;
			if (keyIndex == key.length()) keyIndex = 0;
		}
		return message;
	}
	
	public String getKey() {
		return key;
	}
}
