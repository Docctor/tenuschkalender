/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.cryption;

public class XOREncryption {
	
	private String message;
	private String cryptedMessage;
	
	private String key;
	
	public XOREncryption(String binaryMessage) {
		this.message = binaryMessage;
		key = "";
		for (int i = 0; i < binaryMessage.length(); i++) {
			key = key + ((int) (Math.random()*2));
		}
	}
	
	public XOREncryption(String message, String key) {
		this.message = message;
		this.key = key;
	}

	public String getEncryptedBinaryMessage() {
		
		if (cryptedMessage != null) return cryptedMessage;
		
		cryptedMessage = "";
		int keyIndex = 0;
		for (int i = 0; i < message.length(); i++) {
			int messageNumber = Integer.parseInt(String.valueOf(message.charAt(i)));
			int keyNumber = Integer.parseInt(String.valueOf(key.charAt(keyIndex)));
			if (messageNumber == keyNumber) cryptedMessage = cryptedMessage + "0";
			else cryptedMessage = cryptedMessage + "1";
			
			keyIndex++;
			if (keyIndex == key.length()) keyIndex = 0;
		}
		return cryptedMessage;
	}
	
	public String getKey() {
		return key;
	}
}
