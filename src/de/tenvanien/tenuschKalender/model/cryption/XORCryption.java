/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.cryption;

import java.util.regex.Pattern;

public class XORCryption {
	
	private static String pattern = "^[a-zA-Z0-9" + Pattern.quote("ߴ!\"�$%&/()=?`��{[]}\\^��+��#<,.-�*��'>;:_|~#@| \n")+ "]+$";
	
	public static String convertStringToBinary(String message) {
		char[] chars = message.toCharArray();
		
		String result = "";
		for (char character : chars) {
			result = result + String.format("%8s", Integer.toBinaryString(character)).replace(" ", "0");
		}
		return result;
	}
	
	public static String convertBinaryToString(String binaryMessage) {
		String output = "";
		for (int i = 0; i < binaryMessage.length()/8; i++) {
			int character = Integer.parseInt(binaryMessage.substring(i*8, (i+1)*8), 2);
			output = output + ((char) character);
		}
		return output;
	}
	
	public static boolean isValid(String message) {
		return message.matches(pattern);
	}
}
