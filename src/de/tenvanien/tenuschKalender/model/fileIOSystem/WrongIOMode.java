package de.tenvanien.tenuschKalender.model.fileIOSystem;

@SuppressWarnings("serial")
public class WrongIOMode extends Exception {

	public WrongIOMode(String message) {
		super(message);
	}
}