/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.fileIOSystem;

@SuppressWarnings("serial")
public class UnkownIOMode extends Exception {

	public UnkownIOMode(String message) {
		super(message);
	}
}
