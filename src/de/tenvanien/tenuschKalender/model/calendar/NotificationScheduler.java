/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.calendar;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.systemTray.Notification;

public class NotificationScheduler {

	private Timer timer;
	private TimerTask task;

	public NotificationScheduler() {
		task = new TimerTask() {

			@Override
			public void run() {

				String[] keys = Arrays.copyOf(Event.events.keySet().toArray(), Event.events.keySet().toArray().length, String[].class);
				for (String string : keys) {
					Event event = Event.events.get(string);

					String[] notifications = event.getNotifications();
					for (int i = 0; i < notifications.length; i++) {
						if (notifications[i] == null) break;
						if (event.getNotiIsNoted()[i]) continue;

						int time = Integer.parseInt(notifications[i].substring(0, notifications[i].length()-1));
						String unit = String.valueOf(notifications[i].charAt(notifications[i].length()-1));
						long notiTime = -1;

						switch (unit) {
						case "m":
							notiTime = event.getStart()-time*60*1000;
							break;
						case "h":
							notiTime = event.getStart()-time*60*60*1000;
							break;
						case "d":
							notiTime = event.getStart()-time*60*60*24*1000;
							break;
						default:
							break;
						}

						if (notiTime < System.currentTimeMillis()) {

							try {
								ResultSet userUUID = DatabaseHandler.getInstance().executeCommand("SELECT P_UniqueId AS userUUID FROM t_benutzer WHERE benutzername=\"" + DataConnectionHandler.getInstance().getUsername() + "\";", true);
								userUUID.next();

								String benutzerUUID = userUUID.getString("userUUID");

								ResultSet set = DatabaseHandler.getInstance().executeCommand("SELECT titel, beschreibung FROM t_ereignis WHERE F_Benutzer = \"" + benutzerUUID + "\";", true);

								String titel = "";
								String beschreibung = "";

								while (set.next()) {
									titel = set.getString("titel");
									beschreibung = set.getString("beschreibung");
								}

								Notification notification = new Notification(titel, beschreibung, Integer.MAX_VALUE);
								event.getNotiIsNoted()[i] = true;
								notification.displayTenuschNotification();
								Notification.saveAllNotifications();
							} catch (SQLException | IOException e1) {
								e1.printStackTrace();
							}
						}
					}

				}
			}
		};
	}

	public void stopTimer() {
		if (timer != null) timer.cancel();
	}

	public void startTimer() {
		timer = new Timer();
		timer.scheduleAtFixedRate(task, 0, 30000);
	}
}
