/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.calendar;

import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;

public class Event {
	
	public static volatile Map<String, Event> events = new HashMap<String, Event>();
	
	private volatile UUID uuid;
	private volatile String title;
	private volatile String description;
	private volatile Color color;
	private volatile long start;
	private volatile long end;
	private volatile String[] notifications = new String[5];
	private volatile boolean[] NotiIsNoted = new boolean[5];
	
	
	public Event(UUID uuid, String title, String description, Color color, long start, long end, String[] notifications) {
		intializeNotifyBools();
		this.uuid = uuid;
		this.title = title;
		this.description = description;
		this.color = color;
		this.start = start;
		this.end = end;
		this.notifications = notifications;
	}

	public Event(String title, String description, Color color, long start, long end, String[] notifications) {
		this.uuid = UUID.randomUUID();
		this.title = title;
		this.description = description;
		this.color = color;
		this.start = start;
		this.end = end;
		this.notifications = notifications;
	}
	
	public Event() {
		intializeNotifyBools();
		this.uuid = UUID.randomUUID();
		start = -1;
		end = -1;
	}
	
	public Event(UUID uuid, String[] notifications, String[] notifyBools) {
		intializeNotifyBools();
		this.uuid = uuid;
		for (int i = 0; i < notifications.length; i++) {
			if (notifications[i].equals("null")) notifications[i] = null;
			else this.notifications[i] = notifications[i];
			this.NotiIsNoted[i] = Boolean.parseBoolean(notifyBools[i]);
		}
	}
	
	private void intializeNotifyBools() {
		for (int i = 0; i < 5; i++) {
			NotiIsNoted[i] = false;
		}
	}

	public UUID getUniqueId() {
		return uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public String[] getNotifications() {
		return notifications;
	}

	public void setNotifications(String[] notifications) {
		this.notifications = notifications;
	}
	
	public void addToDatabase() throws SQLException {
		removeFromDatabase();
		ResultSet set = DatabaseHandler.getInstance().executeCommand("SELECT P_UniqueId AS theId FROM t_benutzer WHERE benutzername = \"" + DataConnectionHandler.getInstance().getUsername() +"\";", true);
		set.next();
		String uuidUser = set.getString("theId");
		
		String colorString = color.getRed() + ";" + color.getGreen() + ";" + color.getBlue();
		DatabaseHandler.getInstance().executeCommand("INSERT INTO t_ereignis VALUES (\"" + uuid.toString() + "\", \"" + uuidUser + "\", \"" + title + "\", \"" + colorString + "\", \"" + description + "\", " + start + ", " + end + ");", false);
	}
	
	public void removeFromDatabase() throws SQLException {
		DatabaseHandler.getInstance().executeCommand("DELETE FROM t_ereignis WHERE P_UniqueId=\"" + uuid.toString() + "\";", false);
	}

	public boolean[] getNotiIsNoted() {
		return NotiIsNoted;
	}

	public void setNotiIsNoted(boolean[] notiIsNoted) {
		NotiIsNoted = notiIsNoted;
	}
}
