/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.ImageIcon;

import de.tenvanien.tenuschKalender.main.TenuschKalender;

public class ThemeHandler {
	
	private static ZipInputStream zis;
	
	public ThemeHandler(String theme) {
		Win11LookAndFeel.themeName = theme;
	}
	
	public void loadThemes() throws IOException {
		File path = new File("./themes/");
		if (!path.exists()) return;
		
		File[] themes = path.listFiles();
		for (int i = 0; i < themes.length; i++) {
			
			if (themes[i].isDirectory()) continue;
			if (!themes[i].getName().endsWith(".properties")) continue;
			
			Properties prop = new Properties();
			Reader reader = new FileReader(themes[i]);
			prop.load(reader);
			String fileName = themes[i].getName().replace(".properties", "");
			
			Theme theme = new Theme(fileName);
			theme.setDisplayName(prop.getProperty("name"));
			theme.setPrimaryBackground(convertToColor(prop.getProperty("primaryBackground")));
			theme.setPrimaryFont(convertToFont(prop.getProperty("primaryFont")));
			
			theme.setTextFieldBackground(convertToColor(prop.getProperty("textField.color.background")));
			theme.setTextFieldBorder(convertToColor(prop.getProperty("textField.color.border")));
			theme.setTextFieldSelection(convertToColor(prop.getProperty("textField.color.selection")));
			theme.setTextFieldText(convertToColor(prop.getProperty("textField.color.text")));
			theme.setTextFieldUnderline(convertToColor(prop.getProperty("textField.color.underline")));
			theme.setTextFieldSelectionText(convertToColor(prop.getProperty("textField.color.selectionText")));
			
			theme.setPasswordFieldBackground(convertToColor(prop.getProperty("passwordField.color.background")));
			theme.setPasswordFieldBorder(convertToColor(prop.getProperty("passwordField.color.border")));
			theme.setPasswordFieldSelection(convertToColor(prop.getProperty("passwordField.color.selection")));
			theme.setPasswordFieldText(convertToColor(prop.getProperty("passwordField.color.text")));
			theme.setPasswordFieldUnderline(convertToColor(prop.getProperty("passwordField.color.underline")));
			theme.setPasswordFieldSelectionText(convertToColor(prop.getProperty("passwordField.color.selectionText")));
			
			theme.setEditorPaneBackground(convertToColor(prop.getProperty("editorPane.color.background")));
			theme.setEditorPaneBorder(convertToColor(prop.getProperty("editorPane.color.border")));
			theme.setEditorPaneSelection(convertToColor(prop.getProperty("editorPane.color.selection")));
			theme.setEditorPaneText(convertToColor(prop.getProperty("editorPane.color.text")));
			theme.setEditorPaneUnderline(convertToColor(prop.getProperty("editorPane.color.underline")));
			theme.setEditorPaneSelectionText(convertToColor(prop.getProperty("editorPane.color.selectionText")));
			
			theme.setButtonBackground(convertToColor(prop.getProperty("button.color.background")));
			theme.setButtonBackgroundPressed(convertToColor(prop.getProperty("button.color.background.pressed")));
			theme.setButtonText(convertToColor(prop.getProperty("button.color.text")));
			theme.setButtonBorder(convertToColor(prop.getProperty("button.color.border")));
			theme.setButtonUnderline(convertToColor(prop.getProperty("button.color.underline")));
			
			theme.setPriorityButtonBackground(convertToColor(prop.getProperty("priorityButton.color.background")));
			theme.setPriorityButtonBackgroundPressed(convertToColor(prop.getProperty("priorityButton.color.background.pressed")));
			theme.setPriorityButtonText(convertToColor(prop.getProperty("priorityButton.color.text")));
			theme.setPriorityButtonBorder(convertToColor(prop.getProperty("priorityButton.color.border")));
			theme.setPriorityButtonUnderline(convertToColor(prop.getProperty("priorityButton.color.underline")));
			
			theme.setScrollBarThumb(convertToColor(prop.getProperty("scrollBar.color.thumb")));
			theme.setScrollBarThickness(Integer.parseInt(prop.getProperty("scrollBar.thickness")));
			
			theme.setCheckBoxText(convertToColor(prop.getProperty("checkBox.color.text")));
			theme.setCheckBoxEmpty(loadImage(fileName, "checkBoxEmpty"));
			theme.setCheckBoxFilled(loadImage(fileName, "checkBoxFilled"));
			theme.setCheckBoxEmptyDisabled(loadImage(fileName, "checkBoxEmptyDisabled"));
			theme.setCheckBoxFilledDisabled(loadImage(fileName, "checkBoxFilledDisabled"));
			theme.setCheckBoxPressed(loadImage(fileName, "checkBoxPressed"));
			
			theme.setRadioButtonText(convertToColor(prop.getProperty("radioButton.color.text")));
			theme.setRadioButtonEmpty(loadImage(fileName, "radioButtonEmpty"));
			theme.setRadioButtonFilled(loadImage(fileName, "radioButtonFilled"));
			theme.setRadioButtonEmptyDisabled(loadImage(fileName, "radioButtonEmptyDisabled"));
			theme.setRadioButtonFilledDisabled(loadImage(fileName, "radioButtonFilledDisabled"));
			theme.setRadioButtonPressed(loadImage(fileName, "radioButtonPressed"));
			
			theme.setLabelText(convertToColor(prop.getProperty("label.color.text")));
			
			theme.setPrimaryIcon(convertToImage(loadImage(fileName, "calendar")));
			
			theme.setDatabaseIcon(loadImage(fileName, "database"));
			theme.setUserIcon(loadImage(fileName, "user"));
			
		}
	}
	
	public static void saveDefaultThemes() throws IOException {
		//Pr�ft ob Themes Ordner bereits existiert
		File file = new File("./themes/");
		if (file.exists()) return;
		
		//Legt Zipordner und Zielordner fest
		String fileZip = TenuschKalender.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		File destDir = new File(".");
		
		//Zip Datei lesen
        byte[] buffer = new byte[2048];
        zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
        	//Nur Dateien die im Ordner Themes liegen entpacken
        	if (!zipEntry.getName().startsWith("themes/")) {
        		zipEntry = zis.getNextEntry();
        		continue;
        	}
        	
        	//Pr�fen ob Eintrag Datei oder Ordner ist
        	File newFile = new File(destDir, zipEntry.getName());
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) throw new IOException("Failed to create directory " + newFile);
            } else {
                
                //Dateien abspeichern
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }
	}
	
	private Color convertToColor(String color) {
		String[] values = color.split(":");
		return new Color(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]));
	}
	
	private Font convertToFont(String font) {
		String[] values = font.split(":");
		return new Font(values[0], Font.PLAIN, Integer.parseInt(values[1]));
	}
	
	private ImageIcon loadImage(String folder, String name) throws IOException {
		File file = new File("./themes/" + folder + "/" + name + ".png");
		
		return new ImageIcon(file.getAbsolutePath());
	}
	
	private Image convertToImage(ImageIcon icon) {
		return icon.getImage();
	}
}
