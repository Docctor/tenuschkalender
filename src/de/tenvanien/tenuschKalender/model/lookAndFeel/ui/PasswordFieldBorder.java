/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.border.AbstractBorder;

@SuppressWarnings("serial")
public class PasswordFieldBorder extends AbstractBorder {
	
	private Color color;
	private Color underline;
	private Color background;
	private Color foreground;
	
	public PasswordFieldBorder(Color border, Color underline, Color background, Color foreground) {
		color = border;
		this.underline = underline;
		this.background = background;
		this.foreground = foreground;
	}
	
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
		Graphics2D graphics = (Graphics2D) g;
		Dimension arcs = new Dimension(8,8);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        //Draws the rounded panel with borders.
        graphics.setColor(color);
        graphics.drawRoundRect(0, 0, width-1, height-1, arcs.width, arcs.height);
        graphics.setColor(color);
        graphics.drawRoundRect(0, 0, width-1, height-1, arcs.width, arcs.height);
        
        graphics.setColor(background);
        graphics.drawLine(0, 0, 1, 0);
        graphics.drawLine(0, 1, 0, 1);
        graphics.drawLine(0, height-1, 1, height-1);
        graphics.drawLine(0, height-1, 0, height-2);
        graphics.drawLine(width-2, 0, width-1, 0);
        graphics.drawLine(width-1, 1, width-1, 1);
        graphics.drawLine(width-2, height-1, width-1, height-1);
        graphics.drawLine(width-1, height-2, width-1, height-2);
        
        graphics.setColor(foreground);
        graphics.drawOval(width-24, height/2-4, 12, 12);
        graphics.setColor(background);
        graphics.fillRect(width-24, height/2+1, 13, 8);
        graphics.setColor(foreground);
        graphics.drawOval(width-21, height/2-2, 6, 6);
        
        graphics.setColor(underline);
        graphics.drawLine(1, height-2, width-2, height-2);
        graphics.drawLine(2, height-1, width-3, height-1);
    }
}
