/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.ui;

import java.awt.Adjustable;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.plaf.metal.MetalScrollBarUI;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11ScrollButton;

public class Win11ScrollBarUI extends MetalScrollBarUI {

	private String theme;

	public Win11ScrollBarUI(String theme) {
		this.theme = theme;
	}

	@Override
    protected JButton createDecreaseButton(int orientation) {
		Win11ScrollButton button = new Win11ScrollButton(theme);
        return button;
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
    	Win11ScrollButton button = new Win11ScrollButton(theme);
        return button;
    }

    @Override
    protected void paintDecreaseHighlight(Graphics g) {}

    @Override
    protected void paintIncreaseHighlight(Graphics g) {}

    @Override
    protected void paintThumb (Graphics g, JComponent c, Rectangle r) {
    	Graphics2D graphics = (Graphics2D) g;
    	Dimension arcs = new Dimension(8,8);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Theme.themes.get(theme).getScrollBarThumb());
        JScrollBar scrollBar = (JScrollBar) c;
        int orientation =  scrollBar.getOrientation();
        if (orientation == Adjustable.VERTICAL) {
        	graphics.fillRoundRect((int) r.getX(), (int) r.getY(), (int) r.getWidth()-1, (int) r.getHeight(), arcs.width, arcs.height);
        } else {
        	graphics.fillRoundRect((int) r.getX(), (int) r.getY(), (int) r.getWidth(), (int) r.getHeight()-1, arcs.width, arcs.height);
        }
    }
    @Override
    protected void paintTrack (Graphics g, JComponent c, Rectangle r) {
    	g.setColor(Theme.themes.get(theme).getPrimaryBackground());
        g.fillRect((int) r.getX(), (int) r.getY(), (int) r.getWidth(), (int) r.getHeight());
    }
}
