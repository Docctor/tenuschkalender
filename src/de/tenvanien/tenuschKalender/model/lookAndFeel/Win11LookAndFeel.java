/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel;

import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;

import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.ButtonBorder;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.EditorPaneBorder;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.PasswordFieldBorder;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.TextFieldBorder;

@SuppressWarnings("serial")
public class Win11LookAndFeel extends MetalLookAndFeel {

	public static String themeName;

	public Win11LookAndFeel(String theme) {
		themeName = theme;
	}

	@Override
	public String getName() {
		return "Win11";
	}

	@Override
	public String getID() {
		return "Win11";
	}

	@Override
	public String getDescription() {
		return "A Windows 11 like Look and Feel";
	}

	@Override
	public boolean isNativeLookAndFeel() {
		return false;
	}

	@Override
	public boolean isSupportedLookAndFeel() {
		return true;
	}

	@Override
	protected void initComponentDefaults(UIDefaults table) {
		super.initComponentDefaults(table);

		Theme theme = Theme.themes.get(themeName);

		Border componentCorner = new EmptyBorder(3,10,3,32);
		Border componentButtonCorner = new EmptyBorder(3,10,3,10);
		Border componentEditorPaneCorner = new EmptyBorder(3,10,3,32);

		Border tfBorderRaw = new TextFieldBorder(theme.getTextFieldBorder(), theme.getTextFieldUnderline(), theme.getPrimaryBackground(), theme.getTextFieldText());
		Border textFieldBorder = new CompoundBorder(tfBorderRaw, componentCorner);

		Border pfBorderRaw = new PasswordFieldBorder(theme.getPasswordFieldBorder(), theme.getPasswordFieldUnderline(), theme.getPrimaryBackground(), theme.getPasswordFieldText());
		Border passwordFielBorder = new CompoundBorder(pfBorderRaw, componentCorner);

		Border btnBorderRaw = new ButtonBorder(theme.getButtonBorder(), theme.getButtonUnderline(), theme.getPrimaryBackground());
		Border buttonBorder = new CompoundBorder(btnBorderRaw, componentButtonCorner);
		
		Border editorPaneBorderRaw = new EditorPaneBorder(theme.getEditorPaneBorder(), theme.getEditorPaneUnderline(), theme.getPrimaryBackground());
		Border editorPaneBorder = new CompoundBorder(editorPaneBorderRaw, componentEditorPaneCorner);

		Object[] defaults = {
				"TextField.font", theme.getPrimaryFont(),
				"TextField.selectionBackground", theme.getTextFieldSelection(),
				"TextField.selectionForeground", theme.getTextFieldSelectionText(),
				"TextField.background", theme.getTextFieldBackground(),
				"TextField.foreground", theme.getTextFieldText(),
				"TextField.border", textFieldBorder,

				"PasswordField.font", theme.getPrimaryFont(),
				"PasswordField.selectionBackground", theme.getPasswordFieldSelection(),
				"PasswordField.selectionForeground", theme.getPasswordFieldSelectionText(),
				"PasswordField.background", theme.getPasswordFieldBackground(),
				"PasswordField.foreground", theme.getPasswordFieldText(),
				"PasswordField.border", passwordFielBorder,
				
				"EditorPane.font", theme.getPrimaryFont(),
				"EditorPane.selectionBackground", theme.getEditorPaneSelection(),
				"EditorPane.selectionForeground", theme.getEditorPaneSelectionText(),
				"EditorPane.background", theme.getEditorPaneBackground(),
				"EditorPane.foreground", theme.getEditorPaneText(),
				"EditorPane.border", editorPaneBorder,

				"Button.font", theme.getPrimaryFont(),
				"Button.background", theme.getButtonBackground(),
				"Button.foreground", theme.getButtonText(),
				"Button.select", theme.getButtonBackgroundPressed(),
				"Button.focus", theme.getButtonBackground(),
				"Button.textShiftOffset", 0,
				"Button.border", buttonBorder,

				"ScrollBar.width", theme.getScrollBarThickness(),
				
				"CheckBox.background", theme.getPrimaryBackground(),
				"CheckBox.font", theme.getPrimaryFont(),
				"CheckBox.foreground", theme.getCheckBoxText(),
				"CheckBox.focus", theme.getPrimaryBackground(),
				
				"RadioButton.background", theme.getPrimaryBackground(),
				"RadioButton.font", theme.getPrimaryFont(),
				"RadioButton.foreground", theme.getCheckBoxText(),
				"RadioButton.focus", theme.getPrimaryBackground(),

		};

		table.putDefaults(defaults);
	}

	@Override
	protected void initSystemColorDefaults(UIDefaults table) {
		String[] colors = {
				"windowBorder", "#000000",
				"dark_background", "#202020",
				"dark_ComponentBackground", "#2D2D2D",
				"dark_ComponentBorder", "#303030",
				"dark_ComponentSecondary", "#9F9F9F",
				"dark_Blue", "#60CDFF",
				"dark_Text", "#FFFFFF",

				"light_Background", "#F9F9F9",
				"light_ComponentBackground", "#FDFDFD",
				"light_ComponentBorder", "#EAEAEA",
				"light_ComponentSecondary", "#8A8A8A",
				"light_Blue", "#005FB8",
				"light_Text", "#000000",

				"textMark", "#0078D4"
		};
		loadSystemColors(table, colors, false);
	}
}
