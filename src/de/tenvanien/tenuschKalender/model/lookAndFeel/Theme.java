/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public class Theme {
	
	public static Map<String, Theme> themes;
	
	private String name;
	
	private String displayName;
	
	private Color primaryBackground;
	private Font primaryFont;
	private Image primaryIcon;
	
	private ImageIcon databaseIcon;
	private ImageIcon userIcon;
	
	private Color textFieldBackground;
	private Color textFieldText;
	private Color textFieldSelection;
	private Color textFieldBorder;
	private Color textFieldUnderline;
	private Color textFieldSelectionText;
	
	private Color passwordFieldBackground;
	private Color passwordFieldText;
	private Color passwordFieldSelection;
	private Color passwordFieldBorder;
	private Color passwordFieldUnderline;
	private Color passwordFieldSelectionText;
	
	private Color buttonBackground;
	private Color buttonBackgroundPressed;
	private Color buttonText;
	private Color buttonBorder;
	private Color buttonUnderline;
	
	private Color priorityButtonBackground;
	private Color priorityButtonBackgroundPressed;
	private Color priorityButtonText;
	private Color priorityButtonBorder;
	private Color priorityButtonUnderline;

	private Color editorPaneBackground;
	private Color editorPaneText;
	private Color editorPaneSelection;
	private Color editorPaneBorder;
	private Color editorPaneUnderline;
	private Color editorPaneSelectionText;
	
	private Color scrollBarThumb;
	private int scrollBarThickness;
	
	private Color checkBoxText;
	private ImageIcon checkBoxEmpty, checkBoxFilled, checkBoxEmptyDisabled, checkBoxFilledDisabled, checkBoxPressed;
	
	private Color radioButtonText;
	private ImageIcon radioButtonEmpty, radioButtonFilled, radioButtonEmptyDisabled, radioButtonFilledDisabled, radioButtonPressed;
	
	private Color labelText;

	public Theme(String name) {
		this.name = name;
		if (themes == null) {
			themes = new HashMap<String, Theme>();
		}
		
		themes.put(name, this);
	}


	//GENERALLY
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public Color getPrimaryBackground() {
		return primaryBackground;
	}

	public void setPrimaryBackground(Color primaryBackground) {
		this.primaryBackground = primaryBackground;
	}

	public Font getPrimaryFont() {
		return primaryFont;
	}

	public void setPrimaryFont(Font primaryFont) {
		this.primaryFont = primaryFont;
	}
	
	public Image getPrimaryIcon() {
		return primaryIcon;
	}

	public void setPrimaryIcon(Image primaryIcon) {
		this.primaryIcon = primaryIcon;
	}
	//END OF GENERALLY
	
	//STATIC MEHTHODS
	public static Image resizeImage(Image image, int width, int height) {
		return image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}
	
	public static ImageIcon convertImageToImageIcon(Image image) {
		return new ImageIcon(image);
	}
	//END OF STATIC METHODS
	
	//GENERALLY ICONS
	public ImageIcon getDatabaseIcon() {
			return databaseIcon;
	}

	public void setDatabaseIcon(ImageIcon databaseIcon) {
		this.databaseIcon = databaseIcon;
	}
	//END OF GENERALLY ICONS

	//TEXTFIELD
	public Color getTextFieldBackground() {
		return textFieldBackground;
	}

	public void setTextFieldBackground(Color textFieldBackground) {
		this.textFieldBackground = textFieldBackground;
	}

	public Color getTextFieldText() {
		return textFieldText;
	}

	public void setTextFieldText(Color textFieldText) {
		this.textFieldText = textFieldText;
	}

	public Color getTextFieldSelection() {
		return textFieldSelection;
	}

	public void setTextFieldSelection(Color textFieldSelection) {
		this.textFieldSelection = textFieldSelection;
	}

	public Color getTextFieldBorder() {
		return textFieldBorder;
	}

	public void setTextFieldBorder(Color textFieldBorder) {
		this.textFieldBorder = textFieldBorder;
	}

	public Color getTextFieldUnderline() {
		return textFieldUnderline;
	}

	public void setTextFieldUnderline(Color textFieldUnderline) {
		this.textFieldUnderline = textFieldUnderline;
	}
	
	public Color getTextFieldSelectionText() {
		return textFieldSelectionText;
	}

	public void setTextFieldSelectionText(Color textFieldSelectionText) {
		this.textFieldSelectionText = textFieldSelectionText;
	}
	//END OF TEXTFIELD
	
	//PASSWORDFIELD
	public Color getPasswordFieldBackground() {
		return passwordFieldBackground;
	}

	public void setPasswordFieldBackground(Color passwordFieldBackground) {
		this.passwordFieldBackground = passwordFieldBackground;
	}

	public Color getPasswordFieldText() {
		return passwordFieldText;
	}

	public void setPasswordFieldText(Color passwordFieldText) {
		this.passwordFieldText = passwordFieldText;
	}

	public Color getPasswordFieldSelection() {
		return passwordFieldSelection;
	}

	public void setPasswordFieldSelection(Color passwordFieldSelection) {
		this.passwordFieldSelection = passwordFieldSelection;
	}

	public Color getPasswordFieldBorder() {
		return passwordFieldBorder;
	}

	public void setPasswordFieldBorder(Color passwordFieldBorder) {
		this.passwordFieldBorder = passwordFieldBorder;
	}

	public Color getPasswordFieldUnderline() {
		return passwordFieldUnderline;
	}

	public void setPasswordFieldUnderline(Color passwordFieldUnderline) {
		this.passwordFieldUnderline = passwordFieldUnderline;
	}
	
	public Color getPasswordFieldSelectionText() {
		return passwordFieldSelectionText;
	}

	public void setPasswordFieldSelectionText(Color passwordFieldSelectionText) {
		this.passwordFieldSelectionText = passwordFieldSelectionText;
	}
	//END OF PASSWORDFIELD
	
	//BUTTON
	public Color getButtonBackground() {
		return buttonBackground;
	}

	public void setButtonBackground(Color buttonBackground) {
		this.buttonBackground = buttonBackground;
	}

	public Color getButtonBackgroundPressed() {
		return buttonBackgroundPressed;
	}

	public void setButtonBackgroundPressed(Color buttonBackgroundPressed) {
		this.buttonBackgroundPressed = buttonBackgroundPressed;
	}

	public Color getButtonText() {
		return buttonText;
	}

	public void setButtonText(Color buttonText) {
		this.buttonText = buttonText;
	}

	public Color getButtonBorder() {
		return buttonBorder;
	}

	public void setButtonBorder(Color buttonBorder) {
		this.buttonBorder = buttonBorder;
	}

	public Color getButtonUnderline() {
		return buttonUnderline;
	}

	public void setButtonUnderline(Color buttonUnderline) {
		this.buttonUnderline = buttonUnderline;
	}
	//END OF BUTTON
	
	//PRIORITYBUTTON
	public Color getPriorityButtonBackground() {
		return priorityButtonBackground;
	}

	public void setPriorityButtonBackground(Color priorityButtonBackground) {
		this.priorityButtonBackground = priorityButtonBackground;
	}

	public Color getPriorityButtonBackgroundPressed() {
		return priorityButtonBackgroundPressed;
	}

	public void setPriorityButtonBackgroundPressed(Color priorityButtonBackgroundPressed) {
		this.priorityButtonBackgroundPressed = priorityButtonBackgroundPressed;
	}

	public Color getPriorityButtonText() {
		return priorityButtonText;
	}

	public void setPriorityButtonText(Color priorityButtonText) {
		this.priorityButtonText = priorityButtonText;
	}

	public Color getPriorityButtonBorder() {
		return priorityButtonBorder;
	}

	public void setPriorityButtonBorder(Color priorityButtonBorder) {
		this.priorityButtonBorder = priorityButtonBorder;
	}

	public Color getPriorityButtonUnderline() {
		return priorityButtonUnderline;
	}

	public void setPriorityButtonUnderline(Color priorityButtonUnderline) {
		this.priorityButtonUnderline = priorityButtonUnderline;
	}
	//END OF PRIORITYBUTTON

	//SCROLLBAR
	public Color getScrollBarThumb() {
		return scrollBarThumb;
	}

	public void setScrollBarThumb(Color scrollBarThumb) {
		this.scrollBarThumb = scrollBarThumb;
	}
	
	public int getScrollBarThickness() {
		return scrollBarThickness;
	}

	public void setScrollBarThickness(int scrollBarThickness) {
		this.scrollBarThickness = scrollBarThickness;
	}
	//END OF SCROLLBAR

	//CHECKBOX
	public Color getCheckBoxText() {
		return checkBoxText;
	}

	public void setCheckBoxText(Color checkBoxText) {
		this.checkBoxText = checkBoxText;
	}

	public ImageIcon getCheckBoxEmpty() {
		return checkBoxEmpty;
	}

	public void setCheckBoxEmpty(ImageIcon checkBoxEmpty) {
		this.checkBoxEmpty = checkBoxEmpty;
	}

	public ImageIcon getCheckBoxFilled() {
		return checkBoxFilled;
	}

	public void setCheckBoxFilled(ImageIcon checkBoxFilled) {
		this.checkBoxFilled = checkBoxFilled;
	}

	public ImageIcon getCheckBoxEmptyDisabled() {
		return checkBoxEmptyDisabled;
	}

	public void setCheckBoxEmptyDisabled(ImageIcon checkBoxEmptyDisabled) {
		this.checkBoxEmptyDisabled = checkBoxEmptyDisabled;
	}

	public ImageIcon getCheckBoxFilledDisabled() {
		return checkBoxFilledDisabled;
	}

	public void setCheckBoxFilledDisabled(ImageIcon checkBoxFilledDisabled) {
		this.checkBoxFilledDisabled = checkBoxFilledDisabled;
	}

	public ImageIcon getCheckBoxPressed() {
		return checkBoxPressed;
	}

	public void setCheckBoxPressed(ImageIcon checkBoxPressed) {
		this.checkBoxPressed = checkBoxPressed;
	}
	//END OF CHECKBOX
	
	//RADIOBUTTON
	public Color getRadioButtonText() {
		return radioButtonText;
	}

	public void setRadioButtonText(Color radioButtonText) {
		this.radioButtonText = radioButtonText;
	}

	public ImageIcon getRadioButtonEmpty() {
		return radioButtonEmpty;
	}

	public void setRadioButtonEmpty(ImageIcon radioButtonEmpty) {
		this.radioButtonEmpty = radioButtonEmpty;
	}

	public ImageIcon getRadioButtonFilled() {
		return radioButtonFilled;
	}

	public void setRadioButtonFilled(ImageIcon radioButtonFilled) {
		this.radioButtonFilled = radioButtonFilled;
	}

	public ImageIcon getRadioButtonEmptyDisabled() {
		return radioButtonEmptyDisabled;
	}

	public void setRadioButtonEmptyDisabled(ImageIcon radioButtonEmptyDisabled) {
		this.radioButtonEmptyDisabled = radioButtonEmptyDisabled;
	}

	public ImageIcon getRadioButtonFilledDisabled() {
		return radioButtonFilledDisabled;
	}

	public void setRadioButtonFilledDisabled(ImageIcon radioButtonFilledDisabled) {
		this.radioButtonFilledDisabled = radioButtonFilledDisabled;
	}

	public ImageIcon getRadioButtonPressed() {
		return radioButtonPressed;
	}

	public void setRadioButtonPressed(ImageIcon radioButtonPressed) {
		this.radioButtonPressed = radioButtonPressed;
	}
	//END OF RADIOBUTTON

	//LABEL
	public Color getLabelText() {
		return labelText;
	}

	public void setLabelText(Color labelText) {
		this.labelText = labelText;
	}
	//END OF LABEL
	
	//EDITORPANE
	public Color getEditorPaneBackground() {
		return editorPaneBackground;
	}

	public void setEditorPaneBackground(Color editorPaneBackground) {
		this.editorPaneBackground = editorPaneBackground;
	}

	public Color getEditorPaneText() {
		return editorPaneText;
	}

	public void setEditorPaneText(Color editorPaneText) {
		this.editorPaneText = editorPaneText;
	}

	public Color getEditorPaneSelection() {
		return editorPaneSelection;
	}

	public void setEditorPaneSelection(Color editorPaneSelection) {
		this.editorPaneSelection = editorPaneSelection;
	}

	public Color getEditorPaneBorder() {
		return editorPaneBorder;
	}

	public void setEditorPaneBorder(Color editorPaneBorder) {
		this.editorPaneBorder = editorPaneBorder;
	}

	public Color getEditorPaneUnderline() {
		return editorPaneUnderline;
	}

	public void setEditorPaneUnderline(Color editorPaneUnderline) {
		this.editorPaneUnderline = editorPaneUnderline;
	}

	public Color getEditorPaneSelectionText() {
		return editorPaneSelectionText;
	}

	public void setEditorPaneSelectionText(Color editorPaneSelectionText) {
		this.editorPaneSelectionText = editorPaneSelectionText;
	}
	//END OF EDITORPANE

	//USER
	public ImageIcon getUserIcon() {
		return userIcon;
	}


	public void setUserIcon(ImageIcon userIcon) {
		this.userIcon = userIcon;
	}
	//END OF USER
}
