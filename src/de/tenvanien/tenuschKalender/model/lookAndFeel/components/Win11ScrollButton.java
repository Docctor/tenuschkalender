/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JButton;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;

@SuppressWarnings("serial")
public class Win11ScrollButton extends JButton {

	private String theme;
	
	public Win11ScrollButton(String theme) {
		super();
		this.theme = theme;
		setBorderPainted(false);
		setContentAreaFilled(false);
        setFocusPainted(false);
        setPreferredSize(new Dimension(0, 0));
        setMinimumSize(new Dimension(0, 0));
        setMaximumSize(new Dimension(0, 0));
//		setUI(new Win11ScrollButtonUI(theme));
	}
	
	@Override
    protected void paintComponent(Graphics g) {
		g.setColor(Theme.themes.get(theme).getPrimaryBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
    }
}
