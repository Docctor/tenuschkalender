/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JEditorPane;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11EditorPane extends JEditorPane {
	
	private String propt;

	public Win11EditorPane(String propt) {
		super();
		
		if (propt == null) {
			this.propt = "";
		} else {
			this.propt = propt;
			setText(propt);
			setForeground(new Color(150, 150, 150));
		}
		initialize();
	}
	
	public Win11EditorPane() {
		super();
		this.propt = "";
		initialize();
	}
	
	private void initialize() {
		addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
            	if (propt.equals("")) return;
            	
                if(getText().isEmpty() || getText().equals(propt)) {
                    setText(propt);
                    setForeground(new Color(150, 150, 150));
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
            	if (propt.equals("")) return;
            	if(getText().equals(propt)) {
            		if (!isEditable()) {
                		setText(propt);
                		setForeground(new Color(150, 150, 150));
                	} else {
                		setText("");
                        setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getEditorPaneText());
                	}
                }
            }
        });
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (propt.equals("")) return;
				if (getText().equals(propt)) {
					java.awt.Toolkit.getDefaultToolkit().beep();
					setForeground(new Color(200, 0, 0));
				} else {
					setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getEditorPaneText());
				}
			}
		});
	}
	
	public String getPropt() {
		return propt;
	}

	public void setPropt(String propt) {
		this.propt = propt;
	}

	public void fillPropt() {
		setText(propt);
		setForeground(new Color(150, 150, 150));
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
	}
	
//	@Override
//	public void paintComponent(Graphics g) {
//
//	}
}
