/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import javax.swing.JScrollBar;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.Win11ScrollBarUI;

@SuppressWarnings("serial")
public class Win11ScrollBar extends JScrollBar {
	
	public Win11ScrollBar() {
		super();
		setUI(new Win11ScrollBarUI(Win11LookAndFeel.themeName));
	}
}
