/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class Win11Button extends JButton {

	public Win11Button() {
		super();
		setFocusPainted(false);
	}
	
	public Win11Button(String text) {
		super(text);
		setFocusPainted(false);
	}
}
