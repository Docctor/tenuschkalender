/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import javax.swing.JRadioButton;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11RadioButton extends JRadioButton {

	public Win11RadioButton() {
		super();
		setDesign();
	}
	
	public Win11RadioButton(String name) {
		super(name);
		setDesign();
	}
	
	private void setDesign() {
		setIcon(Theme.themes.get(Win11LookAndFeel.themeName).getRadioButtonEmpty());
		setSelectedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getRadioButtonFilled());
		setDisabledIcon(Theme.themes.get(Win11LookAndFeel.themeName).getRadioButtonEmptyDisabled());
		setDisabledSelectedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getRadioButtonFilledDisabled());
		setPressedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getRadioButtonPressed());
		setRolloverEnabled(false);
	}
}
