/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.ui.ButtonBorder;

@SuppressWarnings("serial")
public class Win11PriorityButton extends JButton {
	
	public Win11PriorityButton() {
		super();
		setFocusPainted(false);
		initialize();
	}
	
	public Win11PriorityButton(String text) {
		super(text);
		setFocusPainted(false);
		initialize();
	}
	
	private void initialize() {
		Theme theme = Theme.themes.get(Win11LookAndFeel.themeName);
		Border componentButtonCorner = new EmptyBorder(3,10,3,10);
		Border btnBorderRaw = new ButtonBorder(theme.getPriorityButtonBorder(), theme.getPriorityButtonUnderline(), theme.getPrimaryBackground());
		Border buttonBorder = new CompoundBorder(btnBorderRaw, componentButtonCorner);
		setBorder(buttonBorder);
		setBackground(theme.getPriorityButtonBackground());
		setForeground(theme.getPriorityButtonText());
		setContentAreaFilled(false);
	}
	
	@Override
    protected void paintComponent(Graphics g) {
		
		Theme theme = Theme.themes.get(Win11LookAndFeel.themeName);
        if (getModel().isPressed()) {
            g.setColor(theme.getPriorityButtonBackgroundPressed());
        } else {
            g.setColor(theme.getPriorityButtonBackground());
        }
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }
}
