/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import javax.swing.JPanel;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11Panel extends JPanel {
	
	public Win11Panel() {
		super();
		setFont(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont());
		setBackground(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryBackground());
	}
}
