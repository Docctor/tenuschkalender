/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JTextField;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11TextField extends JTextField {

//	private int buttonX = (int) (getSize().getWidth()-30);
//	private int buttonY = 0;
//	private int buttonHeight = (int) getSize().getHeight();
//	private int buttonWidth = 30;

	private Cursor cursorNormal = new Cursor(Cursor.DEFAULT_CURSOR);
	private Cursor cursorText = new Cursor(Cursor.TEXT_CURSOR);
	private String propt;

	public Win11TextField(String propt) {
		super();
		
		if (propt == null) {
			this.propt = "";
		} else {
			this.propt = propt;
			setText(propt);
			setForeground(new Color(150, 150, 150));
		}
		initialize();
	}
	
	public Win11TextField() {
		super();
		this.propt = "";
		initialize();
	}
	
	private void initialize() {
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				int x = getWidth()-26;
				int y = getHeight()/2-8;
				int width = getWidth()-11;
				int height = getHeight()/2+7;

				if (e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height) {
					setCursor(cursorNormal);
				} else {
					setCursor(cursorText);
				}
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int x = getWidth()-24;
				int y = getHeight()/2-6;
				int width = getWidth()-13;
				int height = getHeight()/2+5;


				if (e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height && e.getButton() == MouseEvent.BUTTON1 && (isEditable())) {
					setText("");
				}

			}
			@Override
			public void mouseExited(MouseEvent e) {

			}
			@Override
			public void mouseEntered(MouseEvent e) {
			}
		});
		addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
            	if (propt.equals("")) return;
            	
                if(getText().isEmpty() || getText().equals(propt)) {
                    setText(propt);
                    setForeground(new Color(150, 150, 150));
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
            	if (propt.equals("")) return;
            	if(getText().equals(propt)) {
            		if (!isEditable()) {
                		setText(propt);
                		setForeground(new Color(150, 150, 150));
                	} else {
                		setText("");
                        setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
                	}
                }
            }
        });
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (propt.equals("")) return;
				if (getText().equals(propt)) {
					java.awt.Toolkit.getDefaultToolkit().beep();
					setForeground(new Color(200, 0, 0));
				} else {
					setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
				}
			}
		});
	}
	
	public String getPropt() {
		return propt;
	}

	public void setPropt(String propt) {
		this.propt = propt;
	}
	
	public void fillPropt() {
		setText(propt);
		setForeground(new Color(150, 150, 150));
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
	}

//	@Override
//	public void paintComponent(Graphics g) {
//
//	}
}
