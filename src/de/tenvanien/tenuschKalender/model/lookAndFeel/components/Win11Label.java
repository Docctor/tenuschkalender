/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.util.UUID;

import javax.swing.JLabel;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11Label extends JLabel {

	private boolean isMarked = false;
	private UUID eventUUID;
	
	public Win11Label() {
		super();
		setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getLabelText());
//		setBackground(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryBackground());
		setFont(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont());
	}
	
	public Win11Label(String text) {
		super(text);
		setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getLabelText());
//		setBackground(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryBackground());
		setFont(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont());
	}

	public boolean isMarked() {
		return isMarked;
	}

	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}

	public UUID getEventUUID() {
		return eventUUID;
	}

	public void setEventUUID(UUID eventUUID) {
		this.eventUUID = eventUUID;
	}
	
	
	
	
}
