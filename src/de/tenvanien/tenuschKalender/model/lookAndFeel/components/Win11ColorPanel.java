/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11ColorPanel extends JPanel {
	
	private Color color;
	private boolean isSelected = false;
	private String realName; 
	
	public Win11ColorPanel(Color color) {
		super();
		this.color = color;
		setFont(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont());
		setBackground(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryBackground());
	}
	
	@Override
	public void paintComponent(Graphics g) {
		int width = getWidth();
		int height = getHeight();
		Graphics2D graphics = (Graphics2D) g;
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (isSelected) {
			
			graphics.setColor(new Color(150, 150, 150));
			graphics.fillOval(width/2-30, height/2-30, 60, 60);
		}
		graphics.setColor(color);
		
		graphics.fillOval(width/2-25, height/2-25, 50, 50);
		
	}
	public Color getColor() {
		return color;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
	
}
