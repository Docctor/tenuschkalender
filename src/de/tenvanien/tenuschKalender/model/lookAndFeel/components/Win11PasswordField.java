/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JPasswordField;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11PasswordField extends JPasswordField {

	Cursor cursorNormal = new Cursor(Cursor.DEFAULT_CURSOR);
	Cursor cursorText = new Cursor(Cursor.TEXT_CURSOR);
	
	int c = 0x2022;
	
	private String propt;
	
	public Win11PasswordField(String propt) {
		super();
//		this.propt = propt;
//		setText(propt);
//		setEchoChar((char) 0);
//		setForeground(new Color(150, 150, 150));
//		initialize();
		
		if (propt == null) {
			this.propt = "";
		} else {
			this.propt = propt;
			setText(propt);
			setEchoChar((char) 0);
			setForeground(new Color(150, 150, 150));
		}
		initialize();
	}
	
	public Win11PasswordField() {
		super();
		this.propt = "";
		initialize();
	}
	
	private void initialize() {
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				int x = getWidth()-28;
				int y = getHeight()/2-8;
				int width = getWidth()-8;
				int height = getHeight()/2+8;
				
				if (e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height) {
					setCursor(cursorNormal);
					int length = getPassword().length;
					select(length, length);
					setCaretPosition(length);
				} else {
					setCursor(cursorText);
				}
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if (String.valueOf(getPassword()).equals(propt)) return;
				int x = getWidth()-26;
				int y = getHeight()/2-8;
				int width = getWidth()-11;
				int height = getHeight()/2+7;
				
				if (!(e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height)) {
					setEchoChar((char) c);
				} else {
					int length = getPassword().length;
					select(length, length);
					setCaretPosition(length);
				}
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (String.valueOf(getPassword()).equals(propt)) return;
				int x = getWidth()-24;
				int y = getHeight()/2-6;
				int width = getWidth()-13;
				int height = getHeight()/2+5;
				
				if (e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height && e.getButton() == MouseEvent.BUTTON1) {
					setEchoChar((char) 0);
					int length = getPassword().length;
					select(length, length);
					setCaretPosition(length);
				}
			}
			
			public void mouseReleased(MouseEvent e) {
				if (String.valueOf(getPassword()).equals(propt)) return;
				int x = getWidth()-24;
				int y = getHeight()/2-6;
				int width = getWidth()-13;
				int height = getHeight()/2+5;
				
				if (e.getX() >= x && e.getY() >= y && e.getX() <= width && e.getY() <= height) {
					setEchoChar((char) c);
					int length = getPassword().length;
					select(length, length);
					setCaretPosition(length);
				}
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (String.valueOf(getPassword()).equals(propt)) return;
				setEchoChar((char) c);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (propt.equals("")) return;
				if (String.valueOf(getPassword()).equals(propt)) {
					setForeground(new Color(200, 0, 0));
					java.awt.Toolkit.getDefaultToolkit().beep();
					setEchoChar((char) 0);
				} else {
					setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
					setEchoChar((char) c);
				}
			}
		});
		addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {
            	if (propt.equals("")) return;
                if(String.valueOf(getPassword()).length() == 0 || String.valueOf(getPassword()).equals(propt)) {
                	setEchoChar((char) 0);
                	setForeground(new Color(150, 150, 150));
                    setText(propt);
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
            	if (propt.equals("")) return;
            	if(String.valueOf(getPassword()).equals(propt)) {
            		if (!isEditable()) {
                		setText(propt);
                		setForeground(new Color(150, 150, 150));
                	} else {
                		setText("");
                        setEchoChar((char) c);
                        setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getPasswordFieldText());
                	}
                }
            }
        });
	}
	
	public String getPropt() {
		return propt;
	}

	public void setPropt(String propt) {
		this.propt = propt;
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		if (!text.equals(propt)) {
			setEchoChar((char) c);
			setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getPasswordFieldText());
		}
	}
	
	public void fillPropt() {
		setText(propt);
		setEchoChar((char) 0);
		setForeground(new Color(150, 150, 150));
	}
	
}
