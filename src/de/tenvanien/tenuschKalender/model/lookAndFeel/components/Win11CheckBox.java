/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package de.tenvanien.tenuschKalender.model.lookAndFeel.components;

import javax.swing.JCheckBox;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;

@SuppressWarnings("serial")
public class Win11CheckBox extends JCheckBox {

	public Win11CheckBox() {
		super();
		setDesign();
	}
	
	public Win11CheckBox(String name) {
		super(name);
		setDesign();
	}
	
	private void setDesign() {
		setIcon(Theme.themes.get(Win11LookAndFeel.themeName).getCheckBoxEmpty());
		setSelectedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getCheckBoxFilled());
		setDisabledIcon(Theme.themes.get(Win11LookAndFeel.themeName).getCheckBoxEmptyDisabled());
		setDisabledSelectedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getCheckBoxFilledDisabled());
		setPressedIcon(Theme.themes.get(Win11LookAndFeel.themeName).getCheckBoxPressed());
		setRolloverEnabled(false);
	}
}
