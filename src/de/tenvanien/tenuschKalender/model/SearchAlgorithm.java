package de.tenvanien.tenuschKalender.model;

public class SearchAlgorithm {

	public SearchAlgorithm() {}
	
	public static int searchTitle(String[] arr, String str) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].equals(str)) {
				return i;
			} 
		} return -1;
	}
}
