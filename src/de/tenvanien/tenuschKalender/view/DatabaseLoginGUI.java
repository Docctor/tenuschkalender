package de.tenvanien.tenuschKalender.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11CheckBox;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PasswordField;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PriorityButton;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11TextField;

@SuppressWarnings("serial")
public class DatabaseLoginGUI extends JFrame {

	private Win11Panel contentPane;

	private int screenWidth, screenHeight;

	private Win11TextField tFHost;
	private Win11TextField tFPort;
	private Win11TextField tFDatabase;
	private Win11TextField tFUser;
	private Win11PasswordField pFPassword;
	private JCheckBox chbxRemember;
	private Win11PriorityButton btnConnect;

	private EinstellungsGUI einstellungsGUI;

	public DatabaseLoginGUI(EinstellungsGUI einstellungsGUI) {
		this.einstellungsGUI = einstellungsGUI;
		initialize();
	}

	public DatabaseLoginGUI() {
		initialize();
	}

	private void initialize() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenWidth = (int) screenSize.getWidth();
		screenHeight = (int) screenSize.getHeight();

		setBounds((int) (screenWidth/2-screenHeight*0.5/2), (int) (screenHeight/2-screenHeight*0.7/2), (int) (screenHeight*0.5), (int) (screenHeight*0.7));
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Datenbankverbindung");
		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());


//		int panelWidth = (int) (screenHeight*0.5);
//		int panelHeight = (int) (screenHeight*0.7);

		contentPane = new Win11Panel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

//		int colorOffset = 10;
//		Color borderBackground = new Color(Theme.themes.get(theme).getPrimaryBackground().getRed()-colorOffset, Theme.themes.get(theme).getPrimaryBackground().getGreen()-colorOffset, Theme.themes.get(theme).getPrimaryBackground().getBlue()-colorOffset);

//		Win11Panel panelBottom = new Win11Panel();
//		panelBottom.setBackground(borderBackground);
//		panelBottom.setPreferredSize(new Dimension(panelWidth, 60));
//		contentPane.add(panelBottom, BorderLayout.SOUTH);

//		Win11Panel panelTop = new Win11Panel();
//		panelTop.setPreferredSize(new Dimension(panelWidth, 60));
//		panelTop.setBackground(borderBackground);
//		contentPane.add(panelTop, BorderLayout.NORTH);

		Win11Panel panelCenter = new Win11Panel();
		contentPane.add(panelCenter, BorderLayout.CENTER);
		GridBagLayout gbl_panelCenter = new GridBagLayout();
		gbl_panelCenter.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panelCenter.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelCenter.columnWeights = new double[]{1, 1.0, 1, 1.0, 1, 1};
		gbl_panelCenter.rowWeights = new double[]{1, 1.0, 1, 1, 1, 1, 1, 1, 1.0, 1};
		panelCenter.setLayout(gbl_panelCenter);

		Win11Label lblLogo = new Win11Label("");
		lblLogo.setIcon(Theme.themes.get(Win11LookAndFeel.themeName).getDatabaseIcon());
		GridBagConstraints gbc_lblLogo = new GridBagConstraints();
		gbc_lblLogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogo.anchor = GridBagConstraints.WEST;
		gbc_lblLogo.gridx = 1;
		gbc_lblLogo.gridy = 1;
		panelCenter.add(lblLogo, gbc_lblLogo);

		Win11Label lblInfoText = new Win11Label("<html>Datenbankverbindung</html>");
		lblInfoText.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfoText.setVerticalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblInfoText = new GridBagConstraints();
		gbc_lblInfoText.gridwidth = 3;
		gbc_lblInfoText.fill = GridBagConstraints.BOTH;
		gbc_lblInfoText.gridx = 2;
		gbc_lblInfoText.gridy = 1;
		panelCenter.add(lblInfoText, gbc_lblInfoText);

		tFHost = new Win11TextField("Hostname");
		if (DataConnectionHandler.getInstance().getServerName() != null) tFHost.setText(DataConnectionHandler.getInstance().getServerName());
		GridBagConstraints gbc_tFHost = new GridBagConstraints();
		gbc_tFHost.gridwidth = 4;
		gbc_tFHost.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFHost.gridx = 1;
		gbc_tFHost.gridy = 3;
		panelCenter.add(tFHost, gbc_tFHost);

		tFPort = new Win11TextField("Port");
		if (DataConnectionHandler.getInstance().getPort() != -1) tFPort.setText(DataConnectionHandler.getInstance().getPort() + "");
		GridBagConstraints gbc_tFPort = new GridBagConstraints();
		gbc_tFPort.gridwidth = 4;
		gbc_tFPort.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFPort.gridx = 1;
		gbc_tFPort.gridy = 4;
		panelCenter.add(tFPort, gbc_tFPort);

		tFDatabase = new Win11TextField("Datenbank");
		if (DataConnectionHandler.getInstance().getDatabase() != null) tFDatabase.setText(DataConnectionHandler.getInstance().getDatabase());
		GridBagConstraints gbc_tFDatabase = new GridBagConstraints();
		gbc_tFDatabase.gridwidth = 4;
		gbc_tFDatabase.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFDatabase.gridx = 1;
		gbc_tFDatabase.gridy = 5;
		panelCenter.add(tFDatabase, gbc_tFDatabase);

		tFUser = new Win11TextField("Benutzername");
		if (DataConnectionHandler.getInstance().getDatabaseUser() != null) tFUser.setText(DataConnectionHandler.getInstance().getDatabaseUser());
		GridBagConstraints gbc_tFUser = new GridBagConstraints();
		gbc_tFUser.gridwidth = 4;
		gbc_tFUser.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFUser.gridx = 1;
		gbc_tFUser.gridy = 6;
		panelCenter.add(tFUser, gbc_tFUser);

		pFPassword = new Win11PasswordField("Passwort");
		if (DataConnectionHandler.getInstance().getDatabasePassword() != null) pFPassword.setText(DataConnectionHandler.getInstance().getDatabasePassword());
		GridBagConstraints gbc_pFPassword = new GridBagConstraints();
		gbc_pFPassword.gridwidth = 4;
		gbc_pFPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_pFPassword.gridx = 1;
		gbc_pFPassword.gridy = 7;
		panelCenter.add(pFPassword, gbc_pFPassword);

		JPanel panelButtons = new Win11Panel();
		GridBagConstraints gbc_panelButtons = new GridBagConstraints();
		gbc_panelButtons.gridwidth = 4;
		gbc_panelButtons.fill = GridBagConstraints.BOTH;
		gbc_panelButtons.gridx = 1;
		gbc_panelButtons.gridy = 8;
		panelCenter.add(panelButtons, gbc_panelButtons);

		GridBagLayout gbl_panelButtons = new GridBagLayout();
		gbl_panelButtons.columnWidths = new int[]{0, 0};
		gbl_panelButtons.rowHeights = new int[]{0};
		gbl_panelButtons.columnWeights = new double[]{1, 0};
		gbl_panelButtons.rowWeights = new double[]{1};
		panelButtons.setLayout(gbl_panelButtons);

		chbxRemember = new Win11CheckBox("Speichern");
		chbxRemember.setSelected(true);
		GridBagConstraints gbc_chbxRemember = new GridBagConstraints();
//		gbc_chbxRemember.insets = new Insets(0, 0, 0, 5);
		gbc_chbxRemember.gridx = 0;
		gbc_chbxRemember.gridy = 0;
		gbc_chbxRemember.anchor = GridBagConstraints.WEST;
		panelButtons.add(chbxRemember, gbc_chbxRemember);

		btnConnect = new Win11PriorityButton("Verbinden");
		btnConnect.setPreferredSize(new Dimension(140, 32));
		btnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setComponentsEnabled(false);
//				if (tFHost.getText().equals(tFHost.getPropt()) || tFUser.getText().equals(tFUser.getPropt()) || tFDatabase.getText().equals(tFDatabase.getPropt())) {
//					setComponentsEnabled(true);
//					Toolkit.getDefaultToolkit().beep();
//					return;
//				}

				if (!tFPort.getText().equals(tFPort.getPropt())) {
					try {
						Integer.parseInt(tFPort.getText());
					} catch (NumberFormatException e1) {
						setComponentsEnabled(true);
						Toolkit.getDefaultToolkit().beep();
						return;
					}
				}

				DataConnectionHandler.getInstance().setServerName(tFHost.getText().equals(tFHost.getPropt()) ? "localhost" : tFHost.getText());
				DataConnectionHandler.getInstance().setPort((tFPort.getText().equals(tFPort.getPropt())) ? 3306 : Integer.parseInt(tFPort.getText()));
				DataConnectionHandler.getInstance().setDatabase(tFDatabase.getText().equals(tFDatabase.getPropt()) ? "tenuschkalender" : tFDatabase.getText());
				DataConnectionHandler.getInstance().setDatabaseUser(tFUser.getText().equals(tFUser.getPropt()) ? "root" : tFUser.getText());
				DataConnectionHandler.getInstance().setDatabasePassword(String.valueOf(pFPassword.getPassword()).equals(pFPassword.getPropt()) ? "" : String.valueOf(pFPassword.getPassword()));

				try {
					DatabaseHandler.getInstance().setDataSource(DataConnectionHandler.getInstance().getPool());
					DatabaseHandler.getInstance().reconnect();
					DatabaseHandler.getInstance().testConnection();
					DatabaseHandler.getInstance().saveDefaultTables();
					DataConnectionHandler.getInstance().setUsername(null);
					DataConnectionHandler.getInstance().setPassword(null);
					DataConnectionHandler.getInstance().deleteUserConnection();
					if (TenuschKalender.getTimer() != null) TenuschKalender.getTimer().stopTimer();
				} catch (SQLException e2) {
					setComponentsEnabled(true);
					Toolkit.getDefaultToolkit().beep();
					return;
				}

				if (chbxRemember.isSelected()) {
					try {
						DataConnectionHandler.getInstance().saveDatabaseConnection();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					DataConnectionHandler.getInstance().deleteDatabaseConnection();
				}





				if (einstellungsGUI == null) {
					TenuschKalender.getBenutzerLoginGUI().setVisible(true);
					setVisible(false);
				}
				if (einstellungsGUI != null) {
					einstellungsGUI.getLbl_settingDatabaseInfo().setText(DataConnectionHandler.getInstance().getServerName() + "/" + DataConnectionHandler.getInstance().getDatabase());
					BenutzerLoginGUI login = new BenutzerLoginGUI(einstellungsGUI);
					login.setVisible(true);
					setVisible(false);
					return;
				}
				dispose();
			}
		});
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
//		gbc_btnConnect.fill = GridBagConstraints.BOTH;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 0;
		gbc_btnConnect.anchor = GridBagConstraints.EAST;
		panelButtons.add(btnConnect, gbc_btnConnect);
	}

	public void setComponentsEnabled(boolean value) {
		tFHost.setEditable(value);
		tFPort.setEditable(value);
		tFDatabase.setEditable(value);
		tFUser.setEditable(value);
		pFPassword.setEditable(value);
		chbxRemember.setEnabled(value);
		btnConnect.setEnabled(value);
	}
}
