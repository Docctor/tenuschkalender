package de.tenvanien.tenuschKalender.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultStyledDocument;

import components.DocumentSizeFilter;
import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.calendar.Event;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Button;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11ColorPanel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11EditorPane;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PriorityButton;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11TextField;
import de.tenvanien.tenuschKalender.model.systemTray.Notification;

@SuppressWarnings("serial")
public class EreignisGUI extends JFrame {

	private Win11Label lbl_eventDescriptionNumberCharacter;
	private DefaultStyledDocument doc;
	private Win11Button btn_eventCancel;
	private Win11EditorPane ep_eventDescription;
	private Win11Panel pnl_rightPanel;
	private Win11Panel contentPane;
	private Event event;

	private int day;
	private int month;
	private int year;
	private int startHour;
	private int startMinute;
	private int endHour;
	private int endMinute;

	private Color color;

	private Win11Button lbl_eventEndTime;
	private Win11Button lbl_eventStartTime;

	private String loadedPanel = "";

	@SuppressWarnings("unused")
	private long selectedDate;

	private String[] notification = new String[5];
	private Win11TextField[] notificationTextFields = new Win11TextField[5];
	private Win11Button[] notificationButtons = new Win11Button[5];
	private int notificationActualLength = 0;

	public EreignisGUI(long selectedDate) {
		this.selectedDate = selectedDate;
		SimpleDateFormat simpleDay = new SimpleDateFormat("dd");
		SimpleDateFormat simpleMonth = new SimpleDateFormat("MM");
		SimpleDateFormat simpleYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat simpleHour = new SimpleDateFormat("HH");
		SimpleDateFormat simpleMinute = new SimpleDateFormat("mm");

		long currentTime = System.currentTimeMillis();

		day  = Integer.parseInt(simpleDay.format(selectedDate));
		month = Integer.parseInt(simpleMonth.format(selectedDate));
		year = Integer.parseInt(simpleYear.format(selectedDate));
		startHour = Integer.parseInt(simpleHour.format(currentTime));
		startMinute = Integer.parseInt(simpleMinute.format(currentTime));
		endHour = Integer.parseInt(simpleHour.format(currentTime));
		endMinute = Integer.parseInt(simpleMinute.format(currentTime));

		initialize();
	}

	private void setDate(long selectedDate, long endDate) {
		SimpleDateFormat simpleDay = new SimpleDateFormat("dd");
		SimpleDateFormat simpleMonth = new SimpleDateFormat("MM");
		SimpleDateFormat simpleYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat simpleHour = new SimpleDateFormat("HH");
		SimpleDateFormat simpleMinute = new SimpleDateFormat("mm");

		day  = Integer.parseInt(simpleDay.format(selectedDate));
		month = Integer.parseInt(simpleMonth.format(selectedDate));
		year = Integer.parseInt(simpleYear.format(selectedDate));
		startHour = Integer.parseInt(simpleHour.format(selectedDate));
		startMinute = Integer.parseInt(simpleMinute.format(selectedDate));
		endHour = Integer.parseInt(simpleHour.format(endDate));
		endMinute = Integer.parseInt(simpleMinute.format(endDate));
	}

	public EreignisGUI(Event event) {
		this.event = event;
		if (event.getColor() != null) color = event.getColor();
		setDate(event.getStart(), event.getEnd());
		String[] notifications = event.getNotifications();
		for (int i = 0; i < notifications.length; i++) {
			if (notifications[i] == null) break;
			notification[i] = notifications[i];
			notificationTextFields[i] = getUnitSelectorTextField();
			notificationTextFields[i].setText(notifications[i].substring(0, notifications[i].length()-1));
			String unit = "";

			switch (notification[i].charAt(notification[i].length()-1)) {
			case 'm':
				unit = "Minute/n";
				break;
			case 'h':
				unit = "Stunde/n";
				break;
			case 'd':
				unit = "Tag/e";
				break;
			default:
				unit = "minute/n";
				break;
			}

			notificationButtons[i] = getUnitSelectorButton(unit, i);
			notificationActualLength++;
		}

		initialize();
	}

	private void initialize() {

		if (event == null) {
			setTitle("Ereignis hinzuf�gen");
		} else {
			setTitle("Ereignis bearbeiten");
		}

		if (color == null) color = new Color(0, 168, 255);
		if (notification[0]== null) {
			notification[0] = "10m";
			notificationButtons[0] = getUnitSelectorButton("Minute/n", 0);
			notificationTextFields[0] = getUnitSelectorTextField();
			notificationActualLength = 1;
		}


		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenWidth = (int) screenSize.getWidth();
		int screenHeight = (int) screenSize.getHeight();

		int width = 960;
		int height = 540;
		int x = screenWidth/2-width/2;
		int y = screenHeight/2-height/2;

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(x, y, width, height);
		setResizable(false);
		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());


		contentPane = new Win11Panel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		GridLayout layout = new GridLayout(1, 2);
		contentPane.setLayout(layout);

		setContentPane(contentPane);

		Win11Panel pnl_leftPanel = new Win11Panel();
		pnl_leftPanel.setPreferredSize(new Dimension(800, 0));

		GridBagLayout gbl_pnl_leftPanel = new GridBagLayout();
		gbl_pnl_leftPanel.columnWidths = new int[]{0, 0, 0};
		gbl_pnl_leftPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_pnl_leftPanel.columnWeights = new double[]{1, 0, 1};
		gbl_pnl_leftPanel.rowWeights = new double[]{0, 1, 0, 0, 0, 0};
		pnl_leftPanel.setLayout(gbl_pnl_leftPanel);
		contentPane.add(pnl_leftPanel);

		pnl_rightPanel = new Win11Panel();
		pnl_rightPanel.setPreferredSize(new Dimension(800,0));
		contentPane.add(pnl_rightPanel);

		Win11TextField tf_eventTitle = new Win11TextField("Titel");
		tf_eventTitle.setPreferredSize(new Dimension(0, 0));

		DefaultStyledDocument doc_tf_eventTitle = new DefaultStyledDocument();
		doc_tf_eventTitle.setDocumentFilter(new DocumentSizeFilter(40));
		tf_eventTitle.setDocument(doc_tf_eventTitle);
		if (event == null) {
			tf_eventTitle.fillPropt();
		} else {
			tf_eventTitle.setText(event.getTitle());
		}

		GridBagConstraints gbc_tf_eventTitle = new GridBagConstraints();
		gbc_tf_eventTitle.gridwidth = 2;
		gbc_tf_eventTitle.fill = GridBagConstraints.BOTH;
		gbc_tf_eventTitle.gridx = 0;
		gbc_tf_eventTitle.gridy = 0;
		gbc_tf_eventTitle.insets = new Insets(0, 0, 5, 5);
		pnl_leftPanel.add(tf_eventTitle, gbc_tf_eventTitle);

		Win11Button btn_eventColor = new Win11Button("Farbe");
		btn_eventColor.setPreferredSize(new Dimension(0, 28));
		btn_eventColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loadedPanel.equals("colorPanel")) {
					unloadPanel();
				} else {
					unloadPanel();
					loadColorPanel();
					loadedPanel = "colorPanel";
				}
			}
		});

		GridBagConstraints gbc_btn_eventColor = new GridBagConstraints();
		gbc_btn_eventColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_eventColor.gridx = 2;
		gbc_btn_eventColor.gridy = 0;
		gbc_btn_eventColor.insets = new Insets(0, 0, 5, 0);
		pnl_leftPanel.add(btn_eventColor, gbc_btn_eventColor);

		lbl_eventDescriptionNumberCharacter = new Win11Label("128");
		lbl_eventDescriptionNumberCharacter.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_eventDescriptionNumberCharacter.setFont(new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getStyle(), 12));
		lbl_eventDescriptionNumberCharacter.setForeground(new Color(150, 150, 150));

		GridBagConstraints gbc_lbl_eventDescriptionNumberCharacter = new GridBagConstraints();
		gbc_lbl_eventDescriptionNumberCharacter.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_eventDescriptionNumberCharacter.gridx = 2;
		gbc_lbl_eventDescriptionNumberCharacter.gridy = 2;
		pnl_leftPanel.add(lbl_eventDescriptionNumberCharacter, gbc_lbl_eventDescriptionNumberCharacter);

		ep_eventDescription = new Win11EditorPane("Beschreibung");
		ep_eventDescription.setPreferredSize(new Dimension(0, 0));
		doc = new DefaultStyledDocument();
		doc.setDocumentFilter(new DocumentSizeFilter(128));
		doc.addDocumentListener(new DocumentListener(){
            @Override
            public void changedUpdate(DocumentEvent e) {
            	updateCount();
            }
            @Override
            public void insertUpdate(DocumentEvent e) {
            	updateCount();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
            	updateCount();
            }
        });
		ep_eventDescription.setDocument(doc);

		if (event == null) {
			ep_eventDescription.fillPropt();
		} else {
			ep_eventDescription.setText(event.getDescription());
		}

		GridBagConstraints gbc_ep_eventDescription = new GridBagConstraints();
		gbc_ep_eventDescription.fill = GridBagConstraints.BOTH;
		gbc_ep_eventDescription.gridwidth = 3;
		gbc_ep_eventDescription.gridx = 0;
		gbc_ep_eventDescription.gridy = 1;
		pnl_leftPanel.add(ep_eventDescription, gbc_ep_eventDescription);

		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM HH:mm");
		Calendar calVon = Calendar.getInstance();

		lbl_eventStartTime = new Win11Button();
		lbl_eventStartTime.setPreferredSize(new Dimension(0, 32));
		calVon.set(Calendar.DATE, day);
		calVon.set(Calendar.MONTH, month-1);
		calVon.set(Calendar.YEAR, year);
		calVon.set(Calendar.HOUR_OF_DAY, startHour);
		calVon.set(Calendar.MINUTE, startMinute);

		lbl_eventStartTime.setText("" + dateFormat.format(calVon.getTime()));
		lbl_eventStartTime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (loadedPanel.equals("startTimePanel")) {
					unloadPanel();
				} else {
					unloadPanel();
					loadTimeSelector(true);
					loadedPanel = "startTimePanel";
				}
			}
		});

		GridBagConstraints gbc_lbl_eventStartTime = new GridBagConstraints();
		gbc_lbl_eventStartTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_eventStartTime.gridx = 0;
		gbc_lbl_eventStartTime.gridy = 3;
		gbc_lbl_eventStartTime.insets = new Insets(5, 0, 5, 0);
		pnl_leftPanel.add(lbl_eventStartTime, gbc_lbl_eventStartTime);

		Win11Label lbl_eventTimeToCharacter = new Win11Label(" - ");
		lbl_eventTimeToCharacter.setHorizontalAlignment(SwingConstants.CENTER);
//		lbl_eventTimeToCharacter.setPreferredSize(new Dimension(30, 28));
		GridBagConstraints gbc_lbl_eventTimeToCharacter = new GridBagConstraints();
		gbc_lbl_eventTimeToCharacter.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_eventTimeToCharacter.gridx = 1;
		gbc_lbl_eventTimeToCharacter.gridy = 3;
		gbc_lbl_eventTimeToCharacter.insets = new Insets(5, 0, 5, 0);
		pnl_leftPanel.add(lbl_eventTimeToCharacter, gbc_lbl_eventTimeToCharacter);

		Calendar calBis = Calendar.getInstance();

		calBis.set(Calendar.DATE, day);
		calBis.set(Calendar.MONTH, month-1);
		calBis.set(Calendar.YEAR, year);
		calBis.set(Calendar.HOUR_OF_DAY, endHour);
		calBis.set(Calendar.MINUTE, endMinute);

		lbl_eventEndTime = new Win11Button();
		lbl_eventEndTime.setPreferredSize(new Dimension(0, 32));
		lbl_eventEndTime.setText("" + dateFormat.format(calBis.getTime()));
		lbl_eventEndTime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (loadedPanel.equals("endTimePanel")) {
					unloadPanel();
				} else {
					unloadPanel();

					loadTimeSelector(false);
					loadedPanel = "endTimePanel";
				}
			}
		});

		GridBagConstraints gbc_lbl_eventEndTime = new GridBagConstraints();
		gbc_lbl_eventEndTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_eventEndTime.gridx = 2;
		gbc_lbl_eventEndTime.gridy = 3;
		gbc_lbl_eventEndTime.insets = new Insets(5, 0, 5, 0);
		pnl_leftPanel.add(lbl_eventEndTime, gbc_lbl_eventEndTime);

		Win11Button btn_eventNotification = new Win11Button("Erinnerungen");
		btn_eventNotification.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (loadedPanel.equals("notificationPanel")) {
					unloadPanel();
				} else {
					unloadPanel();

					loadNotificationsPanel();
					loadedPanel = "notificationPanel";
				}

			}
		});
		GridBagConstraints gbc_btn_eventNotification = new GridBagConstraints();
		gbc_btn_eventNotification.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_eventNotification.gridx = 0;
		gbc_btn_eventNotification.gridy = 4;
		gbc_btn_eventNotification.gridwidth = 3;
		gbc_btn_eventNotification.insets = new Insets(0, 0, 50, 0);
		pnl_leftPanel.add(btn_eventNotification, gbc_btn_eventNotification);

//		Win11Button btn_eventRepeat = new Win11Button("Wiederholungen");
//
//		GridBagConstraints gbc_btn_eventRepeat = new GridBagConstraints();
//		gbc_btn_eventRepeat.fill = GridBagConstraints.HORIZONTAL;
//		gbc_btn_eventRepeat.gridx = 2;
//		gbc_btn_eventRepeat.gridy = 4;
//		gbc_btn_eventRepeat.insets = new Insets(0, 0, 50, 0);
//		pnl_leftPanel.add(btn_eventRepeat, gbc_btn_eventRepeat);

		Win11PriorityButton btn_eventSave = new Win11PriorityButton("Speichern");
		btn_eventSave.setPreferredSize(new Dimension(0, 28));
		btn_eventSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				unloadPanel();
				
				if (tf_eventTitle.getText().equals(tf_eventTitle.getPropt())) {
					Toolkit.getDefaultToolkit().beep();
					btn_eventSave.setText("Titel fehlt");
					btn_eventSave.setForeground(new Color(150, 0, 0));
					Thread thread = new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							try {
								btn_eventSave.setText("Speichern");
								btn_eventSave.setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getPriorityButtonText());
							} catch (Exception e1) {}

						}
					});
					thread.start();
					return;
				}

				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.MONTH, month-1);
				cal.set(Calendar.YEAR, year);
				cal.set(Calendar.HOUR_OF_DAY, startHour);
				cal.set(Calendar.MINUTE, startMinute);
				Date startDate = cal.getTime();
				cal.set(Calendar.HOUR_OF_DAY, endHour);
				cal.set(Calendar.MINUTE, endMinute);
				Date endDate = cal.getTime();

				if (event == null) {
					event = new Event((tf_eventTitle.getText().equals(tf_eventTitle.getPropt()) ? "" : tf_eventTitle.getText()), (ep_eventDescription.getText().equals(ep_eventDescription.getPropt()) ? "" : ep_eventDescription.getText()), color, startDate.getTime(), endDate.getTime(), notification);
					try {
						event.addToDatabase();
						Event.events.put(event.getUniqueId().toString(), event);
						Notification.addNotification(event);
						try {
							Notification.saveNotifications();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} else {
					event.setTitle(tf_eventTitle.getText().equals(tf_eventTitle.getPropt()) ? "" : tf_eventTitle.getText());
					event.setDescription(ep_eventDescription.getText().equals(ep_eventDescription.getPropt()) ? "" : ep_eventDescription.getText());
					event.setColor(color);
					event.setStart(startDate.getTime());
					event.setEnd(endDate.getTime());
					event.setNotifications(notification);
					try {
						event.addToDatabase();
						Event.events.put(event.getUniqueId().toString(), event);
						Notification.addNotification(event);
						try {
							Notification.saveAllNotifications();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

				}
				boolean temp = TenuschKalender.getKalenderGUI().isVisible();
				TenuschKalender.getKalenderGUI().dispose();
				TenuschKalender.setKalenderGUI(new KalenderGUI());
				if (temp) {
					TenuschKalender.getKalenderGUI().setVisible(true);
				}
				dispose();
			}
		});

		GridBagConstraints gbc_btn_eventSave = new GridBagConstraints();
		gbc_btn_eventSave.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_eventSave.anchor = GridBagConstraints.SOUTH;
		gbc_btn_eventSave.gridwidth = 2;
		gbc_btn_eventSave.gridx = 0;
		gbc_btn_eventSave.gridy = 5;
		gbc_btn_eventSave.insets = new Insets(0, 0, 0, 5);
		pnl_leftPanel.add(btn_eventSave, gbc_btn_eventSave);

		btn_eventCancel = new Win11Button("Abbrechen");
		btn_eventCancel.setPreferredSize(new Dimension(0, 28));
		btn_eventCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();

			}
		});

		GridBagConstraints gbc_btn_eventCancel = new GridBagConstraints();
		gbc_btn_eventCancel.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_eventCancel.anchor = GridBagConstraints.SOUTH;
		gbc_btn_eventCancel.gridx = 2;
		gbc_btn_eventCancel.gridy = 5;
		gbc_btn_eventCancel.insets = new Insets(0, 0, 0, 0);
		pnl_leftPanel.add(btn_eventCancel, gbc_btn_eventCancel);
	}

	private void updateCount() {
        if (!ep_eventDescription.getPropt().equals(ep_eventDescription.getText())) lbl_eventDescriptionNumberCharacter.setText((128-doc.getLength()) + "");
    }

	private void unloadPanel() {
		contentPane.remove(pnl_rightPanel);
		pnl_rightPanel = null;
		pnl_rightPanel = new Win11Panel();
		pnl_rightPanel.setPreferredSize(new Dimension(800,0));
		contentPane.add(pnl_rightPanel);
		loadedPanel = "";
		validate();
		repaint();
	}

	private void loadColorPanel() {
		pnl_rightPanel.setLayout(new BorderLayout());
		pnl_rightPanel.setBorder(new EmptyBorder(0,10,0,0));

		Win11Label lbl_colorInfo = new Win11Label("W�hlen Sie eine Farbe");
		pnl_rightPanel.add(lbl_colorInfo, BorderLayout.NORTH);

		Win11Panel pnl_colorRightCenter = new Win11Panel();
		pnl_colorRightCenter.setLayout(new GridLayout(5,2));
		pnl_rightPanel.add(pnl_colorRightCenter, BorderLayout.CENTER);

		Color[] colors = new Color[] {
			new Color(0, 168, 255),
			new Color(156, 136, 255),
			new Color(251, 197, 49),
			new Color(76, 209, 55),
			new Color(72, 126, 176),
			new Color(232, 65, 24),
			new Color(232, 67, 147),
			new Color(127, 143, 166),
			new Color(39, 60, 117),
			new Color(53, 59, 72)

		};

		Win11ColorPanel[] lbl_colorTypes = new Win11ColorPanel[10];
		for (int i = 0; i < colors.length; i++) {

			Win11ColorPanel lbl_colorType = new Win11ColorPanel(colors[i]);
			if (color != null) {
				if (lbl_colorType.getColor().getRed() == color.getRed() && lbl_colorType.getColor().getGreen() == color.getGreen() && lbl_colorType.getColor().getBlue() == color.getBlue()) {
					lbl_colorType.setSelected(true);
				}
			}
			lbl_colorTypes[i] = lbl_colorType;
			lbl_colorType.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					color = lbl_colorType.getColor();
					for (Win11ColorPanel win11ColorPanel : lbl_colorTypes) {
						win11ColorPanel.setSelected(false);
					}
					lbl_colorType.setSelected(true);
					validate();
					repaint();
				}
			});
			pnl_colorRightCenter.add(lbl_colorType);
		}
		validate();
		repaint();
	}

	private void loadTimeSelector(boolean isStartTime) {

		GridBagLayout rightPanelLayout = new GridBagLayout();
		rightPanelLayout.columnWidths = new int[] {0};
		rightPanelLayout.rowHeights = new int[] {0, 0, 0};
		rightPanelLayout.columnWeights = new double[] {1};
		rightPanelLayout.rowWeights = new double[] {0, 1, 1};


		pnl_rightPanel.setLayout(rightPanelLayout);
		pnl_rightPanel.setBorder(new EmptyBorder(0,5,0,0));

		Font font = new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize());

		Win11Label titleLabel = new Win11Label();
		if (isStartTime)
			titleLabel.setText("W�hlen Sie die Startzeit");
		else
			titleLabel.setText("W�hlen Sie die Endzeit");
		GridBagConstraints gbg_titleLabel = new GridBagConstraints();
		gbg_titleLabel.gridx = 0;
		gbg_titleLabel.gridy = 0;
		gbg_titleLabel.insets = new Insets(0, 5, 0, 0);
		gbg_titleLabel.fill = GridBagConstraints.HORIZONTAL;
		pnl_rightPanel.add(titleLabel, gbg_titleLabel);

		if (isStartTime) {
			Win11Panel panelDate = new Win11Panel();
			GridBagConstraints gbg_panelHour = new GridBagConstraints();
			gbg_panelHour.gridx = 0;
			gbg_panelHour.gridy = 1;
			gbg_panelHour.fill = GridBagConstraints.BOTH;
			pnl_rightPanel.add(panelDate, gbg_panelHour);

			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
			gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0};
			gridBagLayout.columnWeights = new double[] {1, 1, 0, 1, 0, 1, 1};
			gridBagLayout.rowWeights = new double[] {1, 0, 0, 0, 1};
			panelDate.setLayout(gridBagLayout);

			Win11Label year = new Win11Label();
			year.setText(this.year + "");
			year.setHorizontalAlignment(SwingConstants.CENTER);
			year.setFont(font);
			GridBagConstraints gbg_year = new GridBagConstraints();
			gbg_year.gridx = 5;
			gbg_year.gridy = 2;
			gbg_year.insets = new Insets(0, 0, 5, 0);
			gbg_year.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(year, gbg_year);

			Win11Label day = new Win11Label();
			day.setText(this.day + "");
			day.setHorizontalAlignment(SwingConstants.CENTER);
			day.setFont(font);
			GridBagConstraints gbg_day = new GridBagConstraints();
			gbg_day.gridx = 1;
			gbg_day.gridy = 2;
			gbg_day.insets = new Insets(0, 0, 5, 5);
			gbg_day.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(day, gbg_day);

			Win11Label colonMonth = new Win11Label(".");
			colonMonth.setHorizontalAlignment(SwingConstants.CENTER);
			colonMonth.setFont(font);
			GridBagConstraints gbg_colonMonth = new GridBagConstraints();
			gbg_colonMonth.gridx = 2;
			gbg_colonMonth.gridy = 2;
			gbg_colonMonth.insets = new Insets(0, 0, 0, 5);
			panelDate.add(colonMonth, gbg_colonMonth);

			Win11Label month = new Win11Label();
			month.setText((this.month) + "");
			month.setHorizontalAlignment(SwingConstants.CENTER);
			month.setFont(font);
			GridBagConstraints gbg_month = new GridBagConstraints();
			gbg_month.gridx = 3;
			gbg_month.gridy = 2;
			gbg_month.insets = new Insets(0, 0, 5, 5);
			gbg_month.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(month, gbg_month);

			Win11Button monthUp = new Win11Button("\u25B2");
			monthUp.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					month.setText(dateParser(month.getText(), true, "month", null, null));
					YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year.getText()), Integer.parseInt(month.getText()));
					int daysInMonth = yearMonthObject.lengthOfMonth();
					if (daysInMonth < Integer.parseInt(day.getText()))  {
						day.setText(daysInMonth + "");
					}


					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}
			});
			GridBagConstraints gbg_monthUp = new GridBagConstraints();
			gbg_monthUp.gridx = 3;
			gbg_monthUp.gridy = 1;
			gbg_monthUp.insets = new Insets(0, 0, 5, 5);
			gbg_monthUp.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(monthUp, gbg_monthUp);

			Win11Button monthDown = new Win11Button("\u25BC");
			monthDown.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					month.setText(dateParser(month.getText(), false, "month", null, null));
					YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year.getText()), Integer.parseInt(month.getText()));
					int daysInMonth = yearMonthObject.lengthOfMonth();
					if (daysInMonth < Integer.parseInt(day.getText()))  {
						day.setText(daysInMonth + "");
					}
					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}
			});
			GridBagConstraints gbg_monthDown = new GridBagConstraints();
			gbg_monthDown.gridx = 3;
			gbg_monthDown.gridy = 3;
			gbg_monthDown.insets = new Insets(0, 0, 0, 5);
			gbg_monthDown.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(monthDown, gbg_monthDown);


			Win11Label colonYear = new Win11Label(".");
			colonYear.setHorizontalAlignment(SwingConstants.CENTER);
			colonYear.setFont(font);
			GridBagConstraints gbg_colonYear = new GridBagConstraints();
			gbg_colonYear.gridx = 4;
			gbg_colonYear.gridy = 2;
			gbg_colonYear.insets = new Insets(0, 0, 0, 5);
			panelDate.add(colonYear, gbg_colonYear);

			Win11Button yearUp = new Win11Button("\u25B2");
			yearUp.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					year.setText(dateParser(year.getText(), true, "year", null, null));
					YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year.getText()), Integer.parseInt(month.getText()));
					int daysInMonth = yearMonthObject.lengthOfMonth();
					if (daysInMonth < Integer.parseInt(day.getText()))  {
						day.setText(daysInMonth + "");
					}
					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}
			});
			GridBagConstraints gbg_yearUp = new GridBagConstraints();
			gbg_yearUp.gridx = 5;
			gbg_yearUp.gridy = 1;
			gbg_yearUp.insets = new Insets(0, 0, 5, 0);
			gbg_yearUp.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(yearUp, gbg_yearUp);

			Win11Button yearDown = new Win11Button("\u25BC");
			yearDown.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					year.setText(dateParser(year.getText(), false, "year", null, null));
					YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year.getText()), Integer.parseInt(month.getText()));
					int daysInMonth = yearMonthObject.lengthOfMonth();
					if (daysInMonth < Integer.parseInt(day.getText()))  {
						day.setText(daysInMonth + "");
					}
					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}
			});
			GridBagConstraints gbg_yearDown = new GridBagConstraints();
			gbg_yearDown.gridx = 5;
			gbg_yearDown.gridy = 3;
			gbg_yearDown.insets = new Insets(0, 0, 0, 0);
			gbg_yearDown.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(yearDown, gbg_yearDown);

			Win11Button dayUp = new Win11Button("\u25B2");
			dayUp.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					day.setText(dateParser(day.getText(), true, "day", month.getText(), year.getText()));
					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}

			});
			GridBagConstraints gbg_dayUp = new GridBagConstraints();
			gbg_dayUp.gridx = 1;
			gbg_dayUp.gridy = 1;
			gbg_dayUp.insets = new Insets(0, 0, 5, 5);
			gbg_dayUp.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(dayUp, gbg_dayUp);

			Win11Button dayDown = new Win11Button("\u25BC");
			dayDown.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					day.setText(dateParser(day.getText(), false, "day", month.getText(), year.getText()));
					setDate(Integer.parseInt(day.getText()), Integer.parseInt(month.getText()), Integer.parseInt(year.getText()));
				}
			});
			GridBagConstraints gbg_dayDown = new GridBagConstraints();
			gbg_dayDown.gridx = 1;
			gbg_dayDown.gridy = 3;
			gbg_dayDown.insets = new Insets(0, 0, 0, 5);
			gbg_dayDown.fill = GridBagConstraints.HORIZONTAL;
			panelDate.add(dayDown, gbg_dayDown);

		}


		Win11Panel panelTime = new Win11Panel();
		GridBagConstraints gbg_panelTime = new GridBagConstraints();
		gbg_panelTime.gridx = 0;
		if (!isStartTime) {
			gbg_panelTime.gridy = 1;
			gbg_panelTime.gridheight = 2;
		} else {
			gbg_panelTime.gridy = 2;
		}
		gbg_panelTime.fill = GridBagConstraints.BOTH;
		pnl_rightPanel.add(panelTime, gbg_panelTime);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[] {1, 1, 0, 1, 1};
		gridBagLayout.rowWeights = new double[] {1, 0, 0, 0, 1};
		panelTime.setLayout(gridBagLayout);

		String hourText = "";
		if (isStartTime) {
			if (startHour < 10) {
				hourText = "0" + startHour;
			} else {
				hourText = startHour + "";
			}
		} else {
			if (endHour < 10) {
				hourText = "0" + endHour;
			} else {
				hourText = endHour + "";
			}
		}

		Win11Label hour = new Win11Label(hourText);
		hour.setHorizontalAlignment(SwingConstants.CENTER);
		hour.setFont(font);
		GridBagConstraints gbg_hour = new GridBagConstraints();
		gbg_hour.gridx = 1;
		gbg_hour.gridy = 2;
		gbg_hour.insets = new Insets(0, 0, 0, 5);
		gbg_hour.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(hour, gbg_hour);

		String minuteText = "";
		if (isStartTime) {
			if (startMinute < 10) {
				minuteText = "0" + startMinute;
			} else {
				minuteText = startMinute + "";
			}
		} else {
			if (endMinute < 10) {
				minuteText = "0" + endMinute;
			} else {
				minuteText = endMinute + "";
			}
		}

		Win11Label minute = new Win11Label(minuteText);
		minute.setHorizontalAlignment(SwingConstants.CENTER);
		minute.setFont(font);
		GridBagConstraints gbg_minute = new GridBagConstraints();
		gbg_minute.gridx = 3;
		gbg_minute.gridy = 2;
		gbg_minute.insets = new Insets(0, 0, 0, 5);
		gbg_minute.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(minute, gbg_minute);

		Win11Button hourUp = new Win11Button("\u25B2");
		hourUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				hour.setText(timeParser(hour.getText(), true, "hour", (isStartTime ? -1 : startHour)));
				if (Integer.parseInt(hour.getText()) == startHour && Integer.parseInt(minute.getText()) < startMinute) {
					minute.setText(startMinute + "");
				}

				setTime(isStartTime, Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()));
			}
		});
		GridBagConstraints gbg_hourUp = new GridBagConstraints();
		gbg_hourUp.gridx = 1;
		gbg_hourUp.gridy = 1;
		gbg_hourUp.insets = new Insets(0, 0, 5, 5);
		gbg_hourUp.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(hourUp, gbg_hourUp);

		Win11Button hourDown = new Win11Button("\u25BC");
		hourDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				hour.setText(timeParser(hour.getText(), false, "hour", (isStartTime ? -1 : startHour)));
				if (Integer.parseInt(hour.getText()) == startHour && Integer.parseInt(minute.getText()) < startMinute) {
					minute.setText(startMinute + "");
				}
				setTime(isStartTime, Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()));
			}
		});
		GridBagConstraints gbg_hourDown = new GridBagConstraints();
		gbg_hourDown.gridx = 1;
		gbg_hourDown.gridy = 3;
		gbg_hourDown.insets = new Insets(5, 0, 0, 5);
		gbg_hourDown.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(hourDown, gbg_hourDown);

		Win11Button minuteUp = new Win11Button("\u25B2");
		minuteUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (Integer.parseInt(hour.getText())<=startHour)
					minute.setText(timeParser(minute.getText(), true, "minute", (isStartTime ? -1 : startMinute)));
				else
					minute.setText(timeParser(minute.getText(), true, "minute", -1));
				setTime(isStartTime, Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()));
			}
		});
		GridBagConstraints gbg_minuteUp = new GridBagConstraints();
		gbg_minuteUp.gridx = 3;
		gbg_minuteUp.gridy = 1;
		gbg_minuteUp.insets = new Insets(0, 0, 5, 0);
		gbg_minuteUp.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(minuteUp, gbg_minuteUp);

		Win11Button minuteDown = new Win11Button("\u25BC");
		minuteDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (Integer.parseInt(hour.getText())<=startHour)
					minute.setText(timeParser(minute.getText(), false, "minute", (isStartTime ? -1 : startMinute)));
				else
					minute.setText(timeParser(minute.getText(), false, "minute", -1));
				setTime(isStartTime, Integer.parseInt(hour.getText()), Integer.parseInt(minute.getText()));
			}
		});
		GridBagConstraints gbg_minuteDown = new GridBagConstraints();
		gbg_minuteDown.gridx = 3;
		gbg_minuteDown.gridy = 3;
		gbg_minuteDown.insets = new Insets(5, 0, 0, 0);
		gbg_minuteDown.fill = GridBagConstraints.HORIZONTAL;
		panelTime.add(minuteDown, gbg_minuteDown);

		Win11Label colon = new Win11Label(":");
		colon.setHorizontalAlignment(SwingConstants.CENTER);
		colon.setFont(font);
		GridBagConstraints gbg_colon = new GridBagConstraints();
		gbg_colon.gridx = 2;
		gbg_colon.gridy = 2;
		gbg_colon.insets = new Insets(0, 0, 0, 5);
		panelTime.add(colon, gbg_colon);

		validate();
		repaint();
	}

	private void loadNotificationsPanel() {
		GridBagLayout rightPanelLayout = new GridBagLayout();
		rightPanelLayout.columnWidths = new int[] {0, 0, 0};
		rightPanelLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
		rightPanelLayout.columnWeights = new double[] {1, 1, 1};
		rightPanelLayout.rowWeights = new double[] {0, 1, 0, 0, 0, 0, 0, 0, 1};

		pnl_rightPanel.setLayout(rightPanelLayout);
		pnl_rightPanel.setBorder(new EmptyBorder(0, 10, 5, 5));


		Win11Label title = new Win11Label("Stellen Sie die Erinnerungen ein");

		GridBagConstraints gbg_title = new GridBagConstraints();
		gbg_title.gridx = 0;
		gbg_title.gridy = 0;
		gbg_title.gridwidth = 2;
		gbg_title.fill = GridBagConstraints.HORIZONTAL;
		pnl_rightPanel.add(title, gbg_title);

		for (int i = 0; i < notificationActualLength; i++) {
			Win11Button button = notificationButtons[i];
			Win11TextField textField = notificationTextFields[i];

			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.gridx = 1;
			gbc_button.gridy = 2+i;
			gbc_button.insets = new Insets(5, 0, 0, 5);
			gbc_button.fill = GridBagConstraints.HORIZONTAL;
			pnl_rightPanel.add(button, gbc_button);

			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.gridx = 0;
			gbc_textField.gridy = 2+i;
			gbc_textField.insets = new Insets(5, 0, 0, 5);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			pnl_rightPanel.add(textField, gbc_textField);

			Win11Label text = new Win11Label("davor");
			text.setHorizontalAlignment(SwingConstants.CENTER);
			GridBagConstraints gbc_text = new GridBagConstraints();
			gbc_text.gridx = 2;
			gbc_text.gridy = 2+i;
			gbc_text.insets = new Insets(5, 0, 0, 5);
			gbc_text.fill = GridBagConstraints.HORIZONTAL;
			pnl_rightPanel.add(text, gbc_text);


			if (i+1 == notificationActualLength) {
				textField.requestFocus();
				textField.setCaretPosition(textField.getText().length());
			}
		}

		Win11Button add = new Win11Button("Weitere Erinnerung hinzuf�gen");
		add.setEnabled(true);
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				notification[notificationActualLength] = String.valueOf(10+notificationActualLength*5) + "m";
				notificationButtons[notificationActualLength] = getUnitSelectorButton("Minute/n", notificationActualLength);
				notificationTextFields[notificationActualLength] = getUnitSelectorTextField();

				notificationActualLength++;
				unloadPanel();
				loadNotificationsPanel();
			}
		});
		GridBagConstraints gbc_add = new GridBagConstraints();
		gbc_add.gridx = 0;
		gbc_add.gridy = notificationActualLength+2;
		gbc_add.gridwidth = 2;
		gbc_add.fill = GridBagConstraints.HORIZONTAL;
		gbc_add.insets = new Insets(5, 0, 0, 5);
		pnl_rightPanel.add(add, gbc_add);

		if (notificationActualLength == 5) add.setEnabled(false);

		Win11Button remove = new Win11Button("Entfernen");
		remove.setEnabled(false);
		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				notification[notificationActualLength-1] = null;
				notificationTextFields[notificationActualLength-1] = null;
				notificationButtons[notificationActualLength-1] = null;
				notificationActualLength--;
				unloadPanel();
				loadNotificationsPanel();
			}
		});
		GridBagConstraints gbc_remove = new GridBagConstraints();
		gbc_remove.gridx = 2;
		gbc_remove.gridy = notificationActualLength+2;
		gbc_remove.fill = GridBagConstraints.HORIZONTAL;
		gbc_remove.insets = new Insets(5, 0, 0, 0);
		pnl_rightPanel.add(remove, gbc_remove);

		if (notificationActualLength > 1) remove.setEnabled(true);


		validate();
		repaint();

//		notificationButtons[0] = getUnitSelectorButton();
//		GridBagConstraints gbg_standardButton = new GridBagConstraints();
//		gbg_standardButton.gridx = 1;
//		gbg_standardButton.gridy = 2;
//		gbg_standardButton.insets = new Insets(0, 0, 0, 5);
//		gbg_standardButton.fill = GridBagConstraints.HORIZONTAL;
//		pnl_rightPanel.add(notificationButtons[0], gbg_standardButton);
//
//		notificationTextFields[0] = new Win11TextField();
//		notificationTextFields[0].setPreferredSize(new Dimension(0, 32));
//		notificationTextFields[0].setColumns(1);
//		GridBagConstraints gbg_standardTextField = new GridBagConstraints();
//		gbg_standardTextField.gridx = 0;
//		gbg_standardTextField.gridy = 2;
//		gbg_standardTextField.insets = new Insets(0, 0, 0, 5);
//		gbg_standardTextField.fill = GridBagConstraints.HORIZONTAL;
//		pnl_rightPanel.add(notificationTextFields[0], gbg_standardTextField);
//
//		Win11Label standardText = new Win11Label("davor");
//		standardText.setHorizontalAlignment(JLabel.CENTER);
//		GridBagConstraints gbg_standardText = new GridBagConstraints();
//		gbg_standardText.gridx = 2;
//		gbg_standardText.gridy = 2;
//		gbg_standardText.fill = GridBagConstraints.HORIZONTAL;
//		pnl_rightPanel.add(standardText, gbg_standardText);
//
//		interpretNotification(notificationButtons[0], notificationTextFields[0], notification[0]);
//
//		for (int i = 1; i < notification.length; i++) {
//			if (notification[i] == null) break;
//			notificationButtons[i] = getUnitSelectorButton();
//			GridBagConstraints gbg_standardButton1 = new GridBagConstraints();
//			gbg_standardButton1.gridx = 1;
//			gbg_standardButton1.gridy = i+2;
//			gbg_standardButton1.insets = new Insets(0, 0, 0, 5);
//			gbg_standardButton1.fill = GridBagConstraints.HORIZONTAL;
//			pnl_rightPanel.add(notificationButtons[i], gbg_standardButton1);
//
//			notificationTextFields[i] = new Win11TextField();
//			notificationTextFields[i].setPreferredSize(new Dimension(0, 32));
//			notificationTextFields[i].setColumns(1);
//			GridBagConstraints gbg_standardTextField1 = new GridBagConstraints();
//			gbg_standardTextField1.gridx = 0;
//			gbg_standardTextField1.gridy = i+2;
//			gbg_standardTextField1.insets = new Insets(0, 0, 0, 5);
//			gbg_standardTextField1.fill = GridBagConstraints.HORIZONTAL;
//			pnl_rightPanel.add(notificationTextFields[i], gbg_standardTextField1);
//
//			Win11Label standardText1 = new Win11Label("davor");
//			standardText1.setHorizontalAlignment(JLabel.CENTER);
//			GridBagConstraints gbg_standardText1 = new GridBagConstraints();
//			gbg_standardText1.gridx = 2;
//			gbg_standardText1.gridy = i+2;
//			gbg_standardText1.fill = GridBagConstraints.HORIZONTAL;
//			pnl_rightPanel.add(standardText1, gbg_standardText1);
//
//			interpretNotification(notificationButtons[i], notificationTextFields[i], notification[i]);
//			if (i == 5) return;
//		}
//
//
//
//		Win11Button add = new Win11Button("Weitere Erinnerung hinzuf�gen");
//		GridBagConstraints gbg_add = new GridBagConstraints();
//		gbg_add.gridx = 0;
//		gbg_add.gridy = 3;
//		gbg_add.gridwidth = notificationButtonPosition;
//		gbg_add.fill = GridBagConstraints.HORIZONTAL;
//		gbg_add.insets = new Insets(5, 0, 0, 0);
//		pnl_rightPanel.add(add, gbg_add);
////		notificationButtonPosition = notificationButtonPosition;
//		add.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//
//				notificationButtons[notificationButtonPosition-2] = getUnitSelectorButton();
//				notificationButtons[notificationButtonPosition-2].setPreferredSize(new Dimension(0, 32));
//				GridBagConstraints gbg_button = new GridBagConstraints();
//				gbg_button.gridx = 1;
//				gbg_button.gridy = notificationButtonPosition;
//				gbg_button.insets = new Insets(5, 0, 0, 5);
//				gbg_button.fill = GridBagConstraints.HORIZONTAL;
//				pnl_rightPanel.add(notificationButtons[notificationButtonPosition-2], gbg_button);
//
//				notificationTextFields[notificationButtonPosition-2] = new Win11TextField();
//				notificationTextFields[notificationButtonPosition-2].setColumns(1);
//				notificationTextFields[notificationButtonPosition-2].setPreferredSize(new Dimension(0, 32));
//				GridBagConstraints gbg_textField = new GridBagConstraints();
//				gbg_textField.gridx = 0;
//				gbg_textField.gridy = notificationButtonPosition;
//				gbg_textField.insets = new Insets(5, 0, 0, 5);
//				gbg_textField.fill = GridBagConstraints.HORIZONTAL;
//				pnl_rightPanel.add(notificationTextFields[notificationButtonPosition-2], gbg_textField);
//
//				Win11Label text = new Win11Label("davor");
//				text.setHorizontalAlignment(JLabel.CENTER);
//				GridBagConstraints gbg_text = new GridBagConstraints();
//				gbg_text.gridx = 2;
//				gbg_text.gridy = notificationButtonPosition;
//				gbg_text.insets = new Insets(5, 0, 0, 0);
//				gbg_text.fill = GridBagConstraints.HORIZONTAL;
//				pnl_rightPanel.add(text, gbg_text);
//				pnl_rightPanel.remove(add);
//				if (notificationButtonPosition != 6) {
//					notificationButtonPosition++;
//					GridBagConstraints gbg_add = new GridBagConstraints();
//					gbg_add.gridx = 0;
//					gbg_add.gridy = notificationButtonPosition;
//					gbg_add.fill = GridBagConstraints.HORIZONTAL;
//					gbg_add.gridwidth = 3;
//					gbg_add.insets = new Insets(5, 0, 0, 0);
//					pnl_rightPanel.add(add, gbg_add);
//				}
//
//
//				validate();
//				repaint();
////				notificationTextFields[notificationButtonPosition-2].requestFocus();
//
//			}
//
//		});
	}

	private String unitButtonToNotification(String value) {
		switch (value) {
		case "Minute/n":
			return "m";
		case "Stunde/n":
			return "h";
		case "Tag/e":
			return "d";
		default:
			return "m";
		}
	}

	private Win11TextField getUnitSelectorTextField() {
		Win11TextField field = new Win11TextField();
		field.setPreferredSize(new Dimension(0, 32));
		field.setColumns(1);
		field.setText(String.valueOf(10+notificationActualLength*5));
		field.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				field.setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
				try {
					int number = Integer.parseInt(field.getText());
					Win11Button button = notificationButtons[notificationActualLength-1];
					String unit = unitButtonToNotification(button.getText());
					notification[notificationActualLength-1] = number + unit;
				} catch (NumberFormatException e1) {
					field.setForeground(new Color(200, 0, 0));
					Toolkit.getDefaultToolkit().beep();
					notification[notificationActualLength-1] = "";
				}
			}
		});
		return field;
	}

	private Win11Button getUnitSelectorButton(String unit, int position) {
		Win11Button button = new Win11Button(unit);
		final String[] units = new String[] {
				"Minute/n",
				"Stunde/n",
				"Tag/e",
		};
		button.setPreferredSize(new Dimension(0, 32));
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {


				Win11TextField field = notificationTextFields[position];

				field.setForeground(Theme.themes.get(Win11LookAndFeel.themeName).getTextFieldText());
				try {
					int number = Integer.parseInt(field.getText());
					Win11Button button2 = notificationButtons[notificationActualLength-1];
					String unit = unitButtonToNotification(button2.getText());
					notification[notificationActualLength-1] = number + unit;

					switch (button.getText()) {
					case "Minute/n":
						button.setText(units[1]);
						break;
					case "Stunde/n":
						button.setText(units[2]);
						break;
					case "Tag/e":
						button.setText(units[0]);
						break;
					default:
						button.setText(units[0]);
						break;
					}

				} catch (NumberFormatException e1) {
					field.setForeground(new Color(200, 0, 0));
					Toolkit.getDefaultToolkit().beep();
					notification[notificationActualLength-1] = "";
				}
			}

		});
		return button;
	}

	private String dateParser(String value, boolean isUp, String unit, String month, String year) {
		int valueInt = Integer.parseInt(value);
		switch (unit) {
		case "day":
			YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year), Integer.parseInt(month));
			int daysInMonth = yearMonthObject.lengthOfMonth();
			if (isUp) {
				valueInt++;
				if (valueInt == (daysInMonth+1)) valueInt = 1;

				String output = "";
				if (valueInt < 10)
					output = "0" + valueInt;
				else
					output = valueInt + "";
				return output;
			} else {
				valueInt--;
				if (valueInt == 0) valueInt = daysInMonth;

				String output = "";
				if (valueInt < 10)
					output = "0" + valueInt;
				else
					output = valueInt + "";
				return output;
			}
		case "month":
			if (isUp) {
				valueInt++;
				if (valueInt == 13) valueInt = 1;

				String output = "";
				if (valueInt < 10)
					output = "0" + valueInt;
				else
					output = valueInt + "";
				return output;
			} else {
				valueInt--;
				if (valueInt == 0) valueInt = 12;

				String output = "";
				if (valueInt < 10)
					output = "0" + valueInt;
				else
					output = valueInt + "";
				return output;
			}
		case "year":
			if (isUp) {
				valueInt++;
				if (valueInt == 2100) valueInt = 2099;
			} else {
				valueInt--;
				if (valueInt == 1969) valueInt = 1970;
			}
			return valueInt + "";
		default:
			return null;
		}
	}

	private String timeParser(String value, boolean isUp, String unit, int minimum) {
		int valueInt = Integer.parseInt(value);
		switch (unit) {
		case "hour":
			if (isUp) {
				valueInt++;

				if (minimum == -1) {
					if (valueInt == 24) valueInt = 0;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				} else {
					if (valueInt == 24) valueInt = minimum;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				}
			} else {

				if (minimum == -1) {
					valueInt--;
					if (valueInt == -1) valueInt = 23;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				} else {
					valueInt--;
					if (valueInt == minimum-1) valueInt = 23;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				}
			}
		case "minute":
			if (isUp) {
				valueInt++;
				if (minimum == -1) {

					if (valueInt == 60) valueInt = 0;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				} else {
					if (valueInt == 60) valueInt = minimum;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				}
			} else {
				valueInt--;
				if (minimum == -1) {
					if (valueInt == -1) valueInt = 59;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				} else {
					if (valueInt == minimum-1) valueInt = 59;

					String output = "";
					if (valueInt < 10)
						output = "0" + valueInt;
					else
						output = valueInt + "";
					return output;
				}
			}
		default:
			return null;
		}
	}

	private void setDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;

		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM HH:mm");
		Calendar calendarStart = Calendar.getInstance();

		calendarStart.set(Calendar.DATE, day);
		calendarStart.set(Calendar.MONTH, month-1);
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.HOUR_OF_DAY, startHour);
		calendarStart.set(Calendar.MINUTE, startMinute);

		lbl_eventStartTime.setText("" + dateFormat.format(calendarStart.getTime()));

		Calendar calendarEnd = Calendar.getInstance();

		calendarEnd.set(Calendar.DATE, day);
		calendarEnd.set(Calendar.MONTH, month-1);
		calendarEnd.set(Calendar.YEAR, year);
		calendarEnd.set(Calendar.HOUR_OF_DAY, endHour);
		calendarEnd.set(Calendar.MINUTE, endMinute);

		lbl_eventEndTime.setText("" + dateFormat.format(calendarEnd.getTime()));


	}

	private void setTime(boolean isStartTime, int hour, int minute) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd MMM HH:mm");
		if (isStartTime) {
			this.startHour = hour;
			this.startMinute = minute;
			if (startHour >= endHour) {

				endHour = hour;
				if (startMinute >= endMinute) endMinute = minute;

				Calendar calendar = Calendar.getInstance();

				calendar.set(Calendar.DATE, day);
				calendar.set(Calendar.MONTH, month-1);
				calendar.set(Calendar.YEAR, year);
				calendar.set(Calendar.HOUR_OF_DAY, endHour);
				calendar.set(Calendar.MINUTE, endMinute);

				lbl_eventEndTime.setText("" + dateFormat.format(calendar.getTime()));
			}

			Calendar calendar = Calendar.getInstance();

			calendar.set(Calendar.DATE, day);
			calendar.set(Calendar.MONTH, month-1);
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.HOUR_OF_DAY, startHour);
			calendar.set(Calendar.MINUTE, startMinute);

			lbl_eventStartTime.setText("" + dateFormat.format(calendar.getTime()));
		} else {
			this.endHour = hour;
			this.endMinute = minute;

			Calendar calendar = Calendar.getInstance();

			calendar.set(Calendar.DATE, day);
			calendar.set(Calendar.MONTH, month-1);
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.HOUR_OF_DAY, endHour);
			calendar.set(Calendar.MINUTE, endMinute);

			lbl_eventEndTime.setText("" + dateFormat.format(calendar.getTime()));
		}
	}
}