package de.tenvanien.tenuschKalender.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11CheckBox;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PasswordField;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PriorityButton;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11TextField;
import de.tenvanien.tenuschKalender.model.systemTray.Notification;

@SuppressWarnings("serial")
public class BenutzerLoginGUI extends JFrame {

	private Win11Panel contentPane;

	private EinstellungsGUI einstellungsGUI;

	private int screenWidth, screenHeight;

	public BenutzerLoginGUI(EinstellungsGUI einstellungsGUI) {
		this.einstellungsGUI = einstellungsGUI;
		initialize(null);
	}

	public BenutzerLoginGUI() {
		initialize(null);
	}

	private void initialize(String username) {

		addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	Toolkit.getDefaultToolkit().beep();
            	if (DataConnectionHandler.getInstance().getUsername() == null) System.exit(0);
            }
        });

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenWidth = (int) screenSize.getWidth();
		screenHeight = (int) screenSize.getHeight();

		setBounds((int) (screenWidth/2-screenHeight*0.5/2), (int) (screenHeight/2-screenHeight*0.7/2), (int) (screenHeight*0.5), (int) (screenHeight*0.7));
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Benutzer Login");
		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());

		contentPane = new Win11Panel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		Win11Panel panelCenter = new Win11Panel();

		contentPane.add(panelCenter, BorderLayout.CENTER);

		GridBagLayout gbl_panelCenter = new GridBagLayout();
		gbl_panelCenter.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panelCenter.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panelCenter.columnWeights = new double[]{1, 1, 1, 1, 1, 1};
		gbl_panelCenter.rowWeights = new double[]{1, 0, 0, 0, 1};
		panelCenter.setLayout(gbl_panelCenter);

		Win11Label lblLogo = new Win11Label("");
		lblLogo.setIcon(Theme.themes.get(Win11LookAndFeel.themeName).getUserIcon());

		GridBagConstraints gbc_lblLogo = new GridBagConstraints();
		gbc_lblLogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogo.gridx = 1;
		gbc_lblLogo.gridy = 0;
		panelCenter.add(lblLogo, gbc_lblLogo);

		Win11Label lblInfoText = new Win11Label("Benutzer Login");
		lblInfoText.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfoText.setVerticalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblInfoText = new GridBagConstraints();
		gbc_lblInfoText.gridwidth = 3;
		gbc_lblInfoText.fill = GridBagConstraints.BOTH;
		gbc_lblInfoText.gridx = 2;
		gbc_lblInfoText.gridy = 0;
		panelCenter.add(lblInfoText, gbc_lblInfoText);

		Win11TextField tFUsername = new Win11TextField("Benutzername");
		if (DataConnectionHandler.getInstance().getUsername() != null) tFUsername.setText(DataConnectionHandler.getInstance().getUsername());
		GridBagConstraints gbc_tFHost = new GridBagConstraints();
		gbc_tFHost.gridwidth = 4;
		gbc_tFHost.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFHost.gridx = 1;
		gbc_tFHost.gridy = 1;
		panelCenter.add(tFUsername, gbc_tFHost);


		Win11PasswordField pFPassword = new Win11PasswordField("Passwort");
//		if (DataConnectionHandler.getInstance().getPassword() != null) pFPassword.setText(DataConnectionHandler.getInstance().getPassword());
		GridBagConstraints gbc_pFPassword = new GridBagConstraints();
		gbc_pFPassword.gridwidth = 4;
		gbc_pFPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_pFPassword.gridx = 1;
		gbc_pFPassword.gridy = 2;
		gbc_pFPassword.insets = new Insets(25, 0, 0, 0);
		panelCenter.add(pFPassword, gbc_pFPassword);

		Theme theme = Theme.themes.get(Win11LookAndFeel.themeName);

		Win11Label lbl_accountLoginRegister = new Win11Label("Registrieren");
		lbl_accountLoginRegister.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_accountLoginRegister.setVerticalAlignment(SwingConstants.NORTH);
		lbl_accountLoginRegister.setFont(new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getStyle(), 12));
		lbl_accountLoginRegister.setForeground(theme.getPriorityButtonBackground());

		lbl_accountLoginRegister.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				loadRegister((tFUsername.getText().equals(tFUsername.getPropt())) ? "" : tFUsername.getText());
			}
		});

		GridBagConstraints gbc_lbl_accountLoginRegister = new GridBagConstraints();
		gbc_lbl_accountLoginRegister.gridx = 1;
		gbc_lbl_accountLoginRegister.gridy = 3;
		gbc_lbl_accountLoginRegister.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_accountLoginRegister.anchor = GridBagConstraints.NORTH;
		gbc_lbl_accountLoginRegister.insets = new Insets(5, 0, 150, 0);
		panelCenter.add(lbl_accountLoginRegister, gbc_lbl_accountLoginRegister);

		Win11Panel panelButtons = new Win11Panel();

		GridBagConstraints gbc_panelButtons = new GridBagConstraints();
		gbc_panelButtons.gridwidth = 4;
		gbc_panelButtons.fill = GridBagConstraints.BOTH;
		gbc_panelButtons.gridx = 1;
		gbc_panelButtons.gridy = 4;
//		gbc_panelButtons.insets = new Insets(-50, 0, 0, 0);
		panelCenter.add(panelButtons, gbc_panelButtons);

		GridBagLayout gbl_panelButtons = new GridBagLayout();
		gbl_panelButtons.columnWidths = new int[]{0, 0};
		gbl_panelButtons.rowHeights = new int[]{0};
		gbl_panelButtons.columnWeights = new double[]{1, 0};
		gbl_panelButtons.rowWeights = new double[]{1};
		panelButtons.setLayout(gbl_panelButtons);

		Win11CheckBox chbxRemember = new Win11CheckBox("Speichern");
		chbxRemember.setSelected(true);

		GridBagConstraints gbc_chbxRemember = new GridBagConstraints();
//		gbc_chbxRemember.insets = new Insets(0, 0, 0, 5);
		gbc_chbxRemember.gridx = 0;
		gbc_chbxRemember.gridy = 0;
		gbc_chbxRemember.anchor = GridBagConstraints.WEST;
		panelButtons.add(chbxRemember, gbc_chbxRemember);

		Win11PriorityButton btnConnect = new Win11PriorityButton("Login");
		btnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnConnect.setEnabled(false);


				if (tFUsername.getText().equals(tFUsername.getPropt()) || String.valueOf(pFPassword.getPassword()).equals(pFPassword.getPropt()) || tFUsername.getText().length() > 20 || String.valueOf(pFPassword.getPassword()).length() > 50) {
					Toolkit.getDefaultToolkit().beep();
					btnConnect.setEnabled(true);
					return;
				}

				try {
					ResultSet set = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(benutzername) AS userExisting FROM t_benutzer WHERE benutzername=\"" + tFUsername.getText() + "\" AND passwort=\"" + String.valueOf(pFPassword.getPassword()).hashCode() + "\";", true);
					set.next();
					int usernumber = set.getInt("userExisting");
					if (usernumber == 0) {
						Toolkit.getDefaultToolkit().beep();
						btnConnect.setEnabled(true);
						return;
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
					return;
				}

				DataConnectionHandler.getInstance().setUsername(tFUsername.getText());
				DataConnectionHandler.getInstance().setPassword(String.valueOf(pFPassword.getPassword()));
				if (chbxRemember.isSelected()) {
					try {
						DataConnectionHandler.getInstance().saveUserConnection();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					DataConnectionHandler.getInstance().deleteUserConnection();
				}

				if (TenuschKalender.getKalenderGUI().isVisible()) {
					TenuschKalender.getKalenderGUI().setVisible(false);
				}

				if (einstellungsGUI == null) {
					TenuschKalender.getKalenderGUI().setVisible(true);
					try {
						Notification.loadNotifications();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					TenuschKalender.getTimer().startTimer();
					TenuschKalender.createTenuschSystemTray();
				}
				if (einstellungsGUI != null) {
					einstellungsGUI.getLbl_settingsAccountInfo().setText("Accountauswahl - Konto: " + tFUsername.getText());
					einstellungsGUI.setVisible(true);
				}
				dispose();
			}

		});

		btnConnect.setPreferredSize(new Dimension(140, 32));

		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
//		gbc_btnConnect.fill = GridBagConstraints.BOTH;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 0;
		gbc_btnConnect.anchor = GridBagConstraints.EAST;
		panelButtons.add(btnConnect, gbc_btnConnect);
	}

	public void loadRegister(String username) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenWidth = (int) screenSize.getWidth();
		screenHeight = (int) screenSize.getHeight();

		setBounds((int) (screenWidth/2-screenHeight*0.5/2), (int) (screenHeight/2-screenHeight*0.7/2), (int) (screenHeight*0.5), (int) (screenHeight*0.7));
		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Benutzer Registration");
		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());

		contentPane = new Win11Panel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		Win11Panel panelCenter = new Win11Panel();

		contentPane.add(panelCenter, BorderLayout.CENTER);

		GridBagLayout gbl_panelCenter = new GridBagLayout();
		gbl_panelCenter.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panelCenter.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panelCenter.columnWeights = new double[]{1, 1, 1, 1, 1, 1};
		gbl_panelCenter.rowWeights = new double[]{1, 0, 0, 0, 1};
		panelCenter.setLayout(gbl_panelCenter);

		Win11Label lblLogo = new Win11Label("");
		lblLogo.setIcon(Theme.themes.get(Win11LookAndFeel.themeName).getUserIcon());

		GridBagConstraints gbc_lblLogo = new GridBagConstraints();
		gbc_lblLogo.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogo.gridx = 1;
		gbc_lblLogo.gridy = 0;
		panelCenter.add(lblLogo, gbc_lblLogo);

		Win11Label lblInfoText = new Win11Label("Benutzer Registration");
		lblInfoText.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfoText.setVerticalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblInfoText = new GridBagConstraints();
		gbc_lblInfoText.gridwidth = 3;
		gbc_lblInfoText.fill = GridBagConstraints.BOTH;
		gbc_lblInfoText.gridx = 2;
		gbc_lblInfoText.gridy = 0;
		panelCenter.add(lblInfoText, gbc_lblInfoText);

		Win11TextField tFUsername = new Win11TextField("Benutzername");
		if (username != "") {
			tFUsername.setText(username);
		}
		GridBagConstraints gbc_tFHost = new GridBagConstraints();
		gbc_tFHost.gridwidth = 4;
		gbc_tFHost.fill = GridBagConstraints.HORIZONTAL;
		gbc_tFHost.gridx = 1;
		gbc_tFHost.gridy = 1;
		panelCenter.add(tFUsername, gbc_tFHost);


		Win11PasswordField pf_accountRegisterPasswort = new Win11PasswordField("Passwort");

		GridBagConstraints gbc_pf_accountRegisterPasswort = new GridBagConstraints();
		gbc_pf_accountRegisterPasswort.gridwidth = 4;
		gbc_pf_accountRegisterPasswort.fill = GridBagConstraints.HORIZONTAL;
		gbc_pf_accountRegisterPasswort.gridx = 1;
		gbc_pf_accountRegisterPasswort.gridy = 2;
		gbc_pf_accountRegisterPasswort.insets = new Insets(25, 0, 0, 0);
		panelCenter.add(pf_accountRegisterPasswort, gbc_pf_accountRegisterPasswort);

		Win11PasswordField pf_accountRegisterPasswortConfirm = new Win11PasswordField("Passwort bestštigen");

		GridBagConstraints gbc_pf_accountRegisterPasswortConfirm = new GridBagConstraints();
		gbc_pf_accountRegisterPasswortConfirm.gridwidth = 4;
		gbc_pf_accountRegisterPasswortConfirm.fill = GridBagConstraints.HORIZONTAL;
		gbc_pf_accountRegisterPasswortConfirm.gridx = 1;
		gbc_pf_accountRegisterPasswortConfirm.gridy = 3;
		gbc_pf_accountRegisterPasswortConfirm.insets = new Insets(25, 0, 130, 0);
		panelCenter.add(pf_accountRegisterPasswortConfirm, gbc_pf_accountRegisterPasswortConfirm);

		Win11Panel panelButtons = new Win11Panel();

		GridBagConstraints gbc_panelButtons = new GridBagConstraints();
		gbc_panelButtons.gridwidth = 4;
		gbc_panelButtons.fill = GridBagConstraints.BOTH;
		gbc_panelButtons.gridx = 1;
		gbc_panelButtons.gridy = 4;
//		gbc_panelButtons.insets = new Insets(-50, 0, 0, 0);
		panelCenter.add(panelButtons, gbc_panelButtons);

		GridBagLayout gbl_panelButtons = new GridBagLayout();
		gbl_panelButtons.columnWidths = new int[]{0, 0};
		gbl_panelButtons.rowHeights = new int[]{0};
		gbl_panelButtons.columnWeights = new double[]{1, 0};
		gbl_panelButtons.rowWeights = new double[]{1};
		panelButtons.setLayout(gbl_panelButtons);

//		Win11CheckBox chbxRemember = new Win11CheckBox("Speichern");
//		chbxRemember.setSelected(true);
//
//		GridBagConstraints gbc_chbxRemember = new GridBagConstraints();
//		gbc_chbxRemember.insets = new Insets(0, 0, 0, 5);
//		gbc_chbxRemember.gridx = 0;
//		gbc_chbxRemember.gridy = 0;
//		gbc_chbxRemember.anchor = GridBagConstraints.WEST;
//		panelButtons.add(chbxRemember, gbc_chbxRemember);

		Win11PriorityButton btnConnect = new Win11PriorityButton("Registrieren");
		btnConnect.setPreferredSize(new Dimension(140, 32));

		btnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tFUsername.getText().equals(tFUsername.getPropt()) || String.valueOf(pf_accountRegisterPasswortConfirm.getPassword()).equals(pf_accountRegisterPasswortConfirm.getPropt()) || String.valueOf(pf_accountRegisterPasswort.getPassword()).equals(pf_accountRegisterPasswort.getPropt()) || tFUsername.getText().length() > 20 || String.valueOf(pf_accountRegisterPasswort.getPassword()).length() > 50 || !(String.valueOf(pf_accountRegisterPasswort.getPassword()).equals(String.valueOf(pf_accountRegisterPasswortConfirm.getPassword())))) {
					Toolkit.getDefaultToolkit().beep();
					btnConnect.setEnabled(true);
					return;
				}
				boolean userNotExiting = false;
				try {
					ResultSet set = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(benutzername) AS userExisting FROM t_benutzer WHERE benutzername=\"" + tFUsername.getText() + "\";", true);
					set.next();
					int usernumber = set.getInt("userExisting");
					if (usernumber == 0) {
						userNotExiting = true;
					} else {
						Toolkit.getDefaultToolkit().beep();
						btnConnect.setEnabled(true);
						return;
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
					return;
				}

				if (userNotExiting) {
					try {
						@SuppressWarnings("unused")
						ResultSet set = DatabaseHandler.getInstance().executeCommand("INSERT INTO t_benutzer VALUES (\"" + UUID.randomUUID() + "\", \"" + tFUsername.getText() + "\", \"" + String.valueOf(pf_accountRegisterPasswort.getPassword()).hashCode() + "\");", false);
					} catch (SQLException e1) {
						e1.printStackTrace();
						return;
					}
				}

				initialize(tFUsername.getText());
			}

		});

		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
//		gbc_btnConnect.fill = GridBagConstraints.BOTH;
		gbc_btnConnect.gridx = 1;
		gbc_btnConnect.gridy = 0;
		gbc_btnConnect.anchor = GridBagConstraints.EAST;
		panelButtons.add(btnConnect, gbc_btnConnect);
	}
}
