package de.tenvanien.tenuschKalender.view;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.data.DatabaseHandler;
import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.calendar.Event;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Button;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11ScrollBar;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11TextField;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class KalenderGUI extends JFrame {

	private Win11Panel contentPane, mainPane, contentPane2, pnl_kalenderRightCenter;
	private List<Win11Label> days = new ArrayList<>();
	@SuppressWarnings("unused")
	private List<Win11Label> borders = new ArrayList<>();
	@SuppressWarnings("unused")
	private int startDay, endDay = 31;
	private int itterator = 0;
	private int n, m = 0;
	private int numberRows;
	private DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("EEEE, MMMM dd, yyyy HH:mm");
	private DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MMMM uuuu");
	private DateTimeFormatter dtf3 = DateTimeFormatter.ofPattern("MM");
	private DateTimeFormatter dtf4 = DateTimeFormatter.ofPattern("yyyy");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
	private int eventDay, eventMonth, eventYear, eventSelectedDate = -1;
	private LocalDateTime now = LocalDateTime.now();
	private LocalDateTime changed;
	private Calendar c1 = Calendar.getInstance();
	private Calendar c2 = Calendar.getInstance();
	private int jahr = c1.get(Calendar.YEAR);
	private Font font = new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize());

	private Win11Label lblMonth;
	private Win11Label lblCurrentDate;
	private Win11Button btn_kalenderRightNorthAddEvent;
	private Event event;
	private UUID eventUUID;
	
	private ResultSet set;

	public KalenderGUI() {
		Calendar c3 = Calendar.getInstance();
		eventMonth = c3.get(Calendar.MONTH);
		eventYear = c3.get(Calendar.YEAR);

		try {
			ResultSet numberRowsSet = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(*) AS numberRows FROM t_ereignis WHERE F_Benutzer = \"" + DataConnectionHandler.getInstance().getUsername() + "\";", true);
			numberRowsSet.next();
			numberRows = numberRowsSet.getInt("numberRows");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		setTitle("TenuschKalender");
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				TenuschKalender.getKalenderGUI().setVisible(false);
			}
		});

		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());
		setResizable(false);
		setBounds(100, 100, 375*2, 350);
		mainPane = new Win11Panel();
		mainPane.setBounds(0, 0, 375*2, 350);
		mainPane.setLayout(new GridLayout(1,2));
		setContentPane(mainPane);

		contentPane = new Win11Panel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setPreferredSize(new Dimension(375, 350));
//		setContentPane(contentPane);

		contentPane.setLayout(new MigLayout("", "[45.00][45.00][45.00][45.00][45.00][45.00][45.00]", "[][16.00][][][][][][][][]"));

		lblCurrentDate = new Win11Label(dtf1.format(now));
		contentPane.add(lblCurrentDate, "cell 0 0 7 1");

//		JSeparator separator1 = new JSeparator();
//		contentPane.add(separator1, "flowx,cell 0 1 7 1");

		lblMonth = new Win11Label(dtf2.format(now));
		contentPane.add(lblMonth, "cell 0 2 5 1");

		Win11Button btnUp = new Win11Button("\u25B2");
		btnUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				monthDown();
			}
		});
		contentPane.add(btnUp, "cell 5 2");

		Win11Button btnDown = new Win11Button("\u25BC");
		btnDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				monthUp();
			}
		});
		contentPane.add(btnDown, "cell 6 2");

		Win11Label lblSu = new Win11Label("So");
		contentPane.add(lblSu, "cell 0 3,alignx center");

		Win11Label lblMo = new Win11Label("Mo");
		contentPane.add(lblMo, "cell 1 3,alignx center");

		Win11Label lblTu = new Win11Label("Di");
		contentPane.add(lblTu, "cell 2 3,alignx center");

		Win11Label lblWe = new Win11Label("Mi");
		contentPane.add(lblWe, "cell 3 3,alignx center");

		Win11Label lblTh = new Win11Label("Do");
		contentPane.add(lblTh, "cell 4 3,alignx center");

		Win11Label lblFr = new Win11Label("Fr");
		contentPane.add(lblFr, "cell 5 3,alignx center");

		Win11Label lblSa = new Win11Label("Sa");
		contentPane.add(lblSa, "cell 6 3,alignx center");

		JSeparator separator = new JSeparator();
		separator.setForeground(Color.CYAN);
		contentPane.add(separator, "cell 0 1 7 1,growx");

		c1.set(jahr, now.getMonthValue(), 0);
		c2.set(jahr, now.getMonthValue()-1, 0);
		createDaysOfMonth();
		addMouseWheelFunction();
		mainPane.add(contentPane);

		contentPane2 = new Win11Panel();
		contentPane2.setPreferredSize(new Dimension(375, 350));
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new BorderLayout());

		Win11Panel pnl_kalenderRightNorth = new Win11Panel();

		GridBagLayout gbl_pnl_kalenderRightRight = new GridBagLayout();
		gbl_pnl_kalenderRightRight.columnWidths = new int[] {1,1};
		gbl_pnl_kalenderRightRight.rowHeights = new int[] {0};
		gbl_pnl_kalenderRightRight.columnWeights = new double[] {0,0};
		gbl_pnl_kalenderRightRight.rowWeights = new double[] {0};
		pnl_kalenderRightNorth.setLayout(gbl_pnl_kalenderRightRight);

		btn_kalenderRightNorthAddEvent = new Win11Button("Ereignis hinzufügen");

		btn_kalenderRightNorthAddEvent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (btn_kalenderRightNorthAddEvent.getText().equals("Ereignis hinzufügen")) {
					
				
					if (eventSelectedDate != -1) {
	
	
						Calendar calendar = Calendar.getInstance();
						calendar.set(Calendar.DAY_OF_MONTH, eventSelectedDate);
						calendar.set(Calendar.MONTH, eventMonth);
						calendar.set(Calendar.YEAR, eventYear);
						calendar.set(Calendar.HOUR_OF_DAY, 0);
						calendar.set(Calendar.MINUTE, 0);
	
						EreignisGUI gui = new EreignisGUI(calendar.getTimeInMillis());
						gui.setVisible(true);
					}else {
						EreignisGUI gui = new EreignisGUI(System.currentTimeMillis());
						gui.setVisible(true);
					}
				} else {
					EreignisGUI gui = new EreignisGUI(event);
					gui.setVisible(true);
				}
			}

		});

		GridBagConstraints gbc_btn_kalenderRightNorthAddEvent = new GridBagConstraints();
		gbc_btn_kalenderRightNorthAddEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_kalenderRightNorthAddEvent.gridx = 0;
		gbc_btn_kalenderRightNorthAddEvent.gridy = 0;
		gbc_btn_kalenderRightNorthAddEvent.insets = new Insets(0, 0, 0, 10);
		pnl_kalenderRightNorth.add(btn_kalenderRightNorthAddEvent, gbc_btn_kalenderRightNorthAddEvent);

		Win11TextField tf_kalenderRightNorthSearchEvent = new Win11TextField("Ereignis Suchen");
		tf_kalenderRightNorthSearchEvent.setColumns(8);

		tf_kalenderRightNorthSearchEvent.addKeyListener(new KeyAdapter() {


			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					pnl_kalenderRightCenter.removeAll();
					validate();
					repaint();

					try {
						ResultSet userUUID = DatabaseHandler.getInstance().executeCommand("SELECT P_UniqueId AS userUUID FROM t_benutzer WHERE benutzername=\"" + DataConnectionHandler.getInstance().getUsername() + "\";", true);
						userUUID.next();

						String benutzerUUID = userUUID.getString("userUUID");

						ResultSet numberRowsSet = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(*) AS numberRows FROM t_ereignis WHERE F_Benutzer = \"" + benutzerUUID + "\" AND titel = \"" + tf_kalenderRightNorthSearchEvent.getText() + "\";", true);

						numberRowsSet.next();
						numberRows = numberRowsSet.getInt("numberRows");

						set = DatabaseHandler.getInstance().executeCommand("SELECT * FROM t_ereignis WHERE F_Benutzer = \"" + benutzerUUID + "\" AND titel = \"" + tf_kalenderRightNorthSearchEvent.getText() + "\";", true);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

					if (numberRows > 0) {



						GridBagLayout gbl_pnl_kalenderRightCenter = new GridBagLayout();
						gbl_pnl_kalenderRightCenter.columnWidths = new int[] {0, 0, 0};

						int[] rowHeights = new int[numberRows * 3];
						double[] rowWeights = new double[numberRows * 3];

						for (int i = 0; i < rowHeights.length; i++) {
							rowHeights[i] = 0;
							rowWeights[i] = 0;
						}

						gbl_pnl_kalenderRightCenter.rowHeights = rowHeights;
						gbl_pnl_kalenderRightCenter.columnWeights = new double[] {0, 0, 0};
						gbl_pnl_kalenderRightCenter.rowWeights = rowWeights;
						pnl_kalenderRightCenter.setLayout(gbl_pnl_kalenderRightCenter);

						int gbc_lbl_kalenderRightCenterEventGridyNumber = 0;

						for (int k = 0; k < numberRows; k++) {

							try {
								set.next();
								String eventTitel = set.getString("titel");

								Color eventColor;
								String[] colorRGB = set.getString("farbe").split(";");
								eventColor = new Color(Integer.parseInt(colorRGB[0]), Integer.parseInt(colorRGB[1]), Integer.parseInt(colorRGB[2]));

								String eventDescription = set.getString("beschreibung");

								long eventStartTime = set.getLong("startzeit");


								long eventEndTime = set.getLong("endzeit");

								Win11Label lbl_kalenderRightCenterEventTitel = new Win11Label(eventTitel);
								lbl_kalenderRightCenterEventTitel.setFont(font);
								lbl_kalenderRightCenterEventTitel.setForeground(eventColor);
//									lbl_kalenderRightCenterEventTitel.setBorder(BorderFactory.createLineBorder(eventColor));

								GridBagConstraints gbc_lbl_kalenderRightCenterEventTitel = new GridBagConstraints();
								gbc_lbl_kalenderRightCenterEventTitel.fill = GridBagConstraints.HORIZONTAL;
								gbc_lbl_kalenderRightCenterEventTitel.gridx = 0;
								gbc_lbl_kalenderRightCenterEventTitel.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber;
								gbc_lbl_kalenderRightCenterEventTitel.insets = new Insets(0, 0, 0, 10);
								pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventTitel, gbc_lbl_kalenderRightCenterEventTitel);





								Win11Label lbl_kalenderRightCenterEventTime = new Win11Label(dateFormat.format(eventStartTime) + " - " + dateFormat.format(eventEndTime));

								GridBagConstraints gbc_lbl_kalenderRightCenterEventTime = new GridBagConstraints();
								gbc_lbl_kalenderRightCenterEventTime.fill = GridBagConstraints.HORIZONTAL;
								gbc_lbl_kalenderRightCenterEventTime.gridx = 0;
								gbc_lbl_kalenderRightCenterEventTime.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber + 1;
								pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventTime, gbc_lbl_kalenderRightCenterEventTime);


								Win11Label lbl_kalenderRightCenterEventDescription = new Win11Label(eventDescription);

								GridBagConstraints gbc_lbl_kalenderRightCenterEventDescription = new GridBagConstraints();
								gbc_lbl_kalenderRightCenterEventDescription.fill = GridBagConstraints.HORIZONTAL;
								gbc_lbl_kalenderRightCenterEventDescription.gridwidth = 3;
								gbc_lbl_kalenderRightCenterEventDescription.gridx = 0;
								gbc_lbl_kalenderRightCenterEventDescription.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber + 2;
								pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventDescription, gbc_lbl_kalenderRightCenterEventDescription);


								gbc_lbl_kalenderRightCenterEventGridyNumber += 3;

							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
						validate();
						repaint();
					} else {
						pnl_kalenderRightCenter.removeAll();
						validate();
						repaint();
					}


				}

			}

		});

		GridBagConstraints gbc_tf_kalenderRightNorthSearchEvent = new GridBagConstraints();
		gbc_tf_kalenderRightNorthSearchEvent.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_kalenderRightNorthSearchEvent.gridx = 1;
		gbc_tf_kalenderRightNorthSearchEvent.gridy = 0;
		pnl_kalenderRightNorth.add(tf_kalenderRightNorthSearchEvent, gbc_tf_kalenderRightNorthSearchEvent);

		contentPane2.add(pnl_kalenderRightNorth, BorderLayout.NORTH);

		pnl_kalenderRightCenter = new Win11Panel();

		JScrollPane sp_kalenderRightCenter = new JScrollPane(pnl_kalenderRightCenter);
		sp_kalenderRightCenter.setBorder(new EmptyBorder(0, 0, 0, 0));

		Win11ScrollBar sb_kalenderRightCenter = new Win11ScrollBar();
		sp_kalenderRightCenter.setVerticalScrollBar(sb_kalenderRightCenter);
		sb_kalenderRightCenter.setUnitIncrement(10);

		Win11ScrollBar sb_kalenderRightCenterHorizontal = new Win11ScrollBar();
		sb_kalenderRightCenterHorizontal.setOrientation(Adjustable.HORIZONTAL);
		sp_kalenderRightCenter.setHorizontalScrollBar(sb_kalenderRightCenterHorizontal);
		sb_kalenderRightCenterHorizontal.setUnitIncrement(10);

		contentPane2.add(sp_kalenderRightCenter, BorderLayout.CENTER);

		mainPane.add(contentPane2);
	}

	private void addMouseWheelFunction() {
		contentPane.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					monthDown();
				} else {
					monthUp();
				}

			}
		});
	}

	private void monthUp() {
		if (jahr >= 2099) return;
		--n;
		++m;
		removeDaysOfMonth();
		if(now.getMonthValue()+m > 12) {
			m = 0;
			n = 0;
			++jahr;
			now = LocalDateTime.of(jahr, 1, 1, 1, 1);
		}
		c1.set(jahr, now.getMonthValue()+m, 0);
		c2.set(jahr, now.getMonthValue()+m-1, 0);
		createDaysOfMonth();
		changed = LocalDateTime.of(now.getYear(), now.getMonthValue()+m, 1, 1, 1);
		lblMonth.setText("" + (changed.format(dtf2)));
		eventMonth = Integer.parseInt(changed.format(dtf3))-1;
		eventYear = Integer.parseInt(changed.format(dtf4));
		lblCurrentDate.setText("" + (dtf1.format(LocalDateTime.now())));
	}

	private void monthDown() {
		if(jahr <= 1970) return;

		--m;
		++n;
		removeDaysOfMonth();
		if(now.getMonthValue()-n-1 < 0) {
			n = 0;
			m = 0;
			--jahr;
			now = LocalDateTime.of(jahr, 12, 1, 1, 1);
		}
		c1.set(jahr, now.getMonthValue()-n, 0);
		c2.set(jahr, now.getMonthValue()-n-1, 0);
		createDaysOfMonth();
		changed = LocalDateTime.of(now.getYear(), now.getMonthValue()-n, 1, 1, 1);
		lblMonth.setText("" + (changed.format(dtf2)));
		eventMonth = Integer.parseInt(changed.format(dtf3))-1;
		eventYear = Integer.parseInt(changed.format(dtf4));
		lblCurrentDate.setText("" + (dtf1.format(LocalDateTime.now())));
	}

	public void createDaysOfMonth() {
		itterator = 0;
		int j = (c2.get(Calendar.DAY_OF_WEEK));
		for(int i = 4; i < 10; i++) {
			for(; j < 7; j++) {
				if((1 + itterator) <= c1.getActualMaximum(Calendar.DAY_OF_MONTH)) {
					days.add(new Win11Label("" + (1 + itterator)));
					String day = days.get(itterator).getText();
					days.get(itterator).setPreferredSize(new Dimension(10, 100));
					days.get(itterator).addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							loadPanel(day);

						}
					});
					contentPane.add(days.get(itterator), "cell " + j +  " " + i + ",alignx center");
				} else {
					break;
				}
				++itterator;
			}
			j = 0;
		}
	}

	public void removeDaysOfMonth() {
		for (Win11Label day : days) {
			day.setVisible(false);
			contentPane.remove(day);
		}
		days.clear();
	}

	private void loadPanel(String day) {
		pnl_kalenderRightCenter.removeAll();
		validate();
		repaint();

		boolean temp = days.get(Integer.parseInt(day)-1).isMarked();

		eventDay = Integer.parseInt(day);

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, eventDay);
		calendar.set(Calendar.MONTH, eventMonth);
		calendar.set(Calendar.YEAR, eventYear);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);

		long startDay = calendar.getTime().getTime();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);

		long endDay = calendar.getTime().getTime();

		try {
			ResultSet userUUID = DatabaseHandler.getInstance().executeCommand("SELECT P_UniqueId AS userUUID FROM t_benutzer WHERE benutzername=\"" + DataConnectionHandler.getInstance().getUsername() + "\";", true);
			userUUID.next();

			String benutzerUUID = userUUID.getString("userUUID");

			ResultSet numberRowsSet = DatabaseHandler.getInstance().executeCommand("SELECT COUNT(*) AS numberRows FROM t_ereignis WHERE F_Benutzer = \"" + benutzerUUID + "\" AND " + startDay + " < startzeit AND endzeit <" + endDay +";", true);

			numberRowsSet.next();
			numberRows = numberRowsSet.getInt("numberRows");

			set = DatabaseHandler.getInstance().executeCommand("SELECT * FROM t_ereignis WHERE F_Benutzer = \"" + benutzerUUID + "\" AND " + startDay + " < startzeit AND endzeit <" + endDay +";", true);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		if (numberRows > 0 && !temp) {



			GridBagLayout gbl_pnl_kalenderRightCenter = new GridBagLayout();
			gbl_pnl_kalenderRightCenter.columnWidths = new int[] {0, 0, 0};

			int[] rowHeights = new int[numberRows * 3];
			double[] rowWeights = new double[numberRows * 3];

			for (int i = 0; i < rowHeights.length; i++) {
				rowHeights[i] = 0;
				rowWeights[i] = 0;
			}

			gbl_pnl_kalenderRightCenter.rowHeights = rowHeights;
			gbl_pnl_kalenderRightCenter.columnWeights = new double[] {0, 0, 0};
			gbl_pnl_kalenderRightCenter.rowWeights = rowWeights;
			pnl_kalenderRightCenter.setLayout(gbl_pnl_kalenderRightCenter);

			int gbc_lbl_kalenderRightCenterEventGridyNumber = 0;

			for (int k = 0; k < numberRows; k++) {

				try {
					set.next();
					String eventTitel = set.getString("titel");

					Color eventColor;
					String[] colorRGB = set.getString("farbe").split(";");
					eventColor = new Color(Integer.parseInt(colorRGB[0]), Integer.parseInt(colorRGB[1]), Integer.parseInt(colorRGB[2]));

					String eventDescription = set.getString("beschreibung");

					long eventStartTime = set.getLong("startzeit");


					long eventEndTime = set.getLong("endzeit");
					
					String[] notifications = new String[5];
					
					
					
					eventUUID = UUID.fromString(set.getString("P_UniqueId"));

					Win11Label lbl_kalenderRightCenterEventTitel = new Win11Label(eventTitel);
					lbl_kalenderRightCenterEventTitel.setEventUUID(eventUUID);
					lbl_kalenderRightCenterEventTitel.setFont(font);
					lbl_kalenderRightCenterEventTitel.setForeground(eventColor);
					lbl_kalenderRightCenterEventTitel.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							
							
							event = new Event(lbl_kalenderRightCenterEventTitel.getEventUUID(), eventTitel, eventDescription, eventColor, eventStartTime, eventEndTime, notifications);
							if (btn_kalenderRightNorthAddEvent.getText().equals("Ereignis hinzufügen")) {
								btn_kalenderRightNorthAddEvent.setText("Ereignis bearbeiten");
							}else {
								btn_kalenderRightNorthAddEvent.setText("Ereignis hinzufügen");
							}
							
						}
					});
					
//									lbl_kalenderRightCenterEventTitel.setBorder(BorderFactory.createLineBorder(eventColor));

					GridBagConstraints gbc_lbl_kalenderRightCenterEventTitel = new GridBagConstraints();
					gbc_lbl_kalenderRightCenterEventTitel.fill = GridBagConstraints.HORIZONTAL;
					gbc_lbl_kalenderRightCenterEventTitel.gridx = 0;
					gbc_lbl_kalenderRightCenterEventTitel.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber;
					gbc_lbl_kalenderRightCenterEventTitel.insets = new Insets(0, 0, 0, 10);
					pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventTitel, gbc_lbl_kalenderRightCenterEventTitel);





					Win11Label lbl_kalenderRightCenterEventTime = new Win11Label(dateFormat.format(eventStartTime) + " - " + dateFormat.format(eventEndTime));

					GridBagConstraints gbc_lbl_kalenderRightCenterEventTime = new GridBagConstraints();
					gbc_lbl_kalenderRightCenterEventTime.fill = GridBagConstraints.HORIZONTAL;
					gbc_lbl_kalenderRightCenterEventTime.gridx = 0;
					gbc_lbl_kalenderRightCenterEventTime.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber + 1;
					pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventTime, gbc_lbl_kalenderRightCenterEventTime);


					Win11Label lbl_kalenderRightCenterEventDescription = new Win11Label(eventDescription);

					GridBagConstraints gbc_lbl_kalenderRightCenterEventDescription = new GridBagConstraints();
					gbc_lbl_kalenderRightCenterEventDescription.fill = GridBagConstraints.HORIZONTAL;
					gbc_lbl_kalenderRightCenterEventDescription.gridwidth = 3;
					gbc_lbl_kalenderRightCenterEventDescription.gridx = 0;
					gbc_lbl_kalenderRightCenterEventDescription.gridy = gbc_lbl_kalenderRightCenterEventGridyNumber + 2;
					pnl_kalenderRightCenter.add(lbl_kalenderRightCenterEventDescription, gbc_lbl_kalenderRightCenterEventDescription);


					gbc_lbl_kalenderRightCenterEventGridyNumber += 3;

				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			validate();
			repaint();
		}





		for (Win11Label day2 : days) {
			day2.setBorder(null);
			day2.setMarked(false);
			eventSelectedDate = -1;
		}

		if (!temp) {
			btn_kalenderRightNorthAddEvent.setText("Ereignis hinzufügen");
			eventSelectedDate = Integer.parseInt(day);
			Border border = BorderFactory.createLineBorder(Color.CYAN);
			days.get(Integer.parseInt(day)-1).setBorder(border);
			days.get(Integer.parseInt(day)-1).setPreferredSize(new Dimension(150, 100));
			days.get(Integer.parseInt(day)-1).setHorizontalAlignment(SwingConstants.CENTER);
			days.get(Integer.parseInt(day)-1).setMarked(!days.get(Integer.parseInt(day)-1).isMarked());
		}

	}
	
}

