package de.tenvanien.tenuschKalender.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.main.TenuschKalender;
import de.tenvanien.tenuschKalender.model.connection.DataConnectionHandler;
import de.tenvanien.tenuschKalender.model.fileIOSystem.IOMode;
import de.tenvanien.tenuschKalender.model.fileIOSystem.IOStream;
import de.tenvanien.tenuschKalender.model.fileIOSystem.UnkownIOMode;
import de.tenvanien.tenuschKalender.model.fileIOSystem.WrongIOMode;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Button;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11CheckBox;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11ColorPanel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11PriorityButton;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11ScrollBar;

@SuppressWarnings("serial")
public class EinstellungsGUI extends JFrame {

	private Win11Panel pnl_rightPanel;

	Win11Label lbl_settingDatabaseInfo;
	Win11Label lbl_settingsAccountInfo;

	EinstellungsGUI thisGUI = this;

	private Win11Label lbl_settingAutostartInfo;


	private Font font = new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize());

	public EinstellungsGUI() {

		Win11Panel contentPane;

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenWidth = (int) screenSize.getWidth();
		int screenHeight = (int) screenSize.getHeight();

		int width = 960;
		int height = 540;
		int x = screenWidth/2-width/2;
		int y = screenHeight/2-height/2;

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(x, y, width, height);
		setResizable(false);
		setTitle("Einstellungen");
		setIconImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon());


		contentPane = new Win11Panel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		GridLayout layout = new GridLayout(1, 2);
		contentPane.setLayout(layout);

		setContentPane(contentPane);

		Win11Panel pnl_leftPanel = new Win11Panel();
		pnl_leftPanel.setPreferredSize(new Dimension(800, 0));

		GridBagLayout gbl_pnl_leftPanel = new GridBagLayout();
		gbl_pnl_leftPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_pnl_leftPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_pnl_leftPanel.columnWeights = new double[]{1, 1, 0, 1};
		gbl_pnl_leftPanel.rowWeights = new double[]{0, 0, 0, 0};
		pnl_leftPanel.setLayout(gbl_pnl_leftPanel);
		contentPane.add(pnl_leftPanel);

		pnl_rightPanel = new Win11Panel();
		pnl_rightPanel.setLayout(new BorderLayout());
		pnl_rightPanel.setBorder(new EmptyBorder(0, 10, 0, 0));
		pnl_rightPanel.setPreferredSize(new Dimension(800,0));
		contentPane.add(pnl_rightPanel);

		Win11Label lbl_settingsTheme = new Win11Label("Theme");
		lbl_settingsTheme.setFont(font);
		lbl_settingsTheme.setBorder(BorderFactory.createLineBorder(Color.CYAN));

		GridBagConstraints gbc_lbl_settingsTheme = new GridBagConstraints();
		gbc_lbl_settingsTheme.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_settingsTheme.gridx = 0;
		gbc_lbl_settingsTheme.gridy = 0;
		pnl_leftPanel.add(lbl_settingsTheme, gbc_lbl_settingsTheme);

		Win11Button btn_settingsTheme = new Win11Button("\u25B6");
		btn_settingsTheme.setFont(new Font("serif", Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize()));

		btn_settingsTheme.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadThemeSettings();
			}
		});

		GridBagConstraints gbc_btn_settingsTheme = new GridBagConstraints();
		gbc_btn_settingsTheme.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsTheme.insets = new Insets(0, -50, 5, 50);
		gbc_btn_settingsTheme.gridx = 3;
		gbc_btn_settingsTheme.gridy = 0;
		pnl_leftPanel.add(btn_settingsTheme, gbc_btn_settingsTheme);

		Win11Label lbl_settingsAccount = new Win11Label("Konto");
		lbl_settingsAccount.setFont(font);
		lbl_settingsAccount.setBorder(BorderFactory.createLineBorder(Color.CYAN));

		GridBagConstraints gbc_lbl_settingsAccount = new GridBagConstraints();
		gbc_lbl_settingsAccount.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_settingsAccount.gridx = 0;
		gbc_lbl_settingsAccount.gridy = 1;
		pnl_leftPanel.add(lbl_settingsAccount, gbc_lbl_settingsAccount);

		Win11Button btn_settingsAccount = new Win11Button("\u25B6");
		btn_settingsAccount.setFont(new Font("serif", Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize()));

		btn_settingsAccount.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadAccountSettings();
			}
		});

		GridBagConstraints gbc_btn_settingsAccount = new GridBagConstraints();
		gbc_btn_settingsAccount.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsAccount.insets = new Insets(0, -50, 5, 50);
		gbc_btn_settingsAccount.gridx = 3;
		gbc_btn_settingsAccount.gridy = 1;
		pnl_leftPanel.add(btn_settingsAccount, gbc_btn_settingsAccount);

		Win11Label lbl_settingsDatabase = new Win11Label("Datenbank");
		lbl_settingsDatabase.setFont(font);
		lbl_settingsDatabase.setBorder(BorderFactory.createLineBorder(Color.CYAN));

		GridBagConstraints gbc_lbl_settingsDatabase = new GridBagConstraints();
		gbc_lbl_settingsDatabase.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_settingsDatabase.gridx = 0;
		gbc_lbl_settingsDatabase.gridy = 2;
		pnl_leftPanel.add(lbl_settingsDatabase, gbc_lbl_settingsDatabase);

		Win11Button btn_settingsDatabase = new Win11Button("\u25B6");
		btn_settingsDatabase.setFont(new Font("serif", Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize()));

		btn_settingsDatabase.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadDatabaseSettings();
			}
		});

		GridBagConstraints gbc_btn_settingsDatabase = new GridBagConstraints();
		gbc_btn_settingsDatabase.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsDatabase.insets = new Insets(0, -50, 5, 50);
		gbc_btn_settingsDatabase.gridx = 3;
		gbc_btn_settingsDatabase.gridy = 2;
		pnl_leftPanel.add(btn_settingsDatabase, gbc_btn_settingsDatabase);

		Win11Label lbl_settingsAutostart = new Win11Label("Autostart");
		lbl_settingsAutostart.setFont(font);
		lbl_settingsAutostart.setBorder(BorderFactory.createLineBorder(Color.CYAN));

		GridBagConstraints gbc_lbl_settingsAutostart = new GridBagConstraints();
		gbc_lbl_settingsAutostart.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_settingsAutostart.gridx = 0;
		gbc_lbl_settingsAutostart.gridy = 3;
		pnl_leftPanel.add(lbl_settingsAutostart, gbc_lbl_settingsAutostart);

		Win11Button btn_settingsAutostart = new Win11Button("\u25B6");
		btn_settingsAutostart.setFont(new Font("serif", Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize()));

		btn_settingsAutostart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadAutostartSettings();
			}
		});

		GridBagConstraints gbc_btn_settingsAutostart = new GridBagConstraints();
		gbc_btn_settingsAutostart.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsAutostart.insets = new Insets(0, -50, 5, 50);
		gbc_btn_settingsAutostart.gridx = 3;
		gbc_btn_settingsAutostart.gridy = 3;
		pnl_leftPanel.add(btn_settingsAutostart, gbc_btn_settingsAutostart);

	}

	public void loadThemeSettings() {
		pnl_rightPanel.removeAll();

		Win11Label lbl_settingsThemeInfo = new Win11Label("Themeauswahl");
		lbl_settingsThemeInfo.setFont(font);
		pnl_rightPanel.add(lbl_settingsThemeInfo, BorderLayout.NORTH);

		Win11Panel pnl_settingsThemeSelection = new Win11Panel();
		pnl_settingsThemeSelection.setLayout(new GridLayout(Theme.themes.keySet().size(), 2));

		String[] keys = Theme.themes.keySet().toArray(new String[Theme.themes.keySet().size()]);

		JScrollPane sp_settingsThemes = new JScrollPane(pnl_settingsThemeSelection);
		sp_settingsThemes.setBorder(new EmptyBorder(0, 0, 0, 0));

		Win11ScrollBar sb_settingsThemes = new Win11ScrollBar();
		sp_settingsThemes.add(sb_settingsThemes);

		for (String key : keys) {

			Win11ColorPanel lbl_settingsThemes = new Win11ColorPanel(Theme.themes.get(key).getPrimaryBackground());
			lbl_settingsThemes.setRealName(key);

			lbl_settingsThemes.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					if (Win11LookAndFeel.themeName != lbl_settingsThemes.getRealName()) {
						try {
							UIManager.setLookAndFeel(new Win11LookAndFeel(lbl_settingsThemes.getRealName()));
							dispose();
							EinstellungsGUI test = new EinstellungsGUI();
							test.setVisible(true);
							boolean temp = TenuschKalender.getKalenderGUI().isVisible();
							TenuschKalender.getKalenderGUI().dispose();
							TenuschKalender.setKalenderGUI(new KalenderGUI());
							if (temp) {
								TenuschKalender.getKalenderGUI().setVisible(true);
							}
						} catch (UnsupportedLookAndFeelException e1) {
							e1.printStackTrace();
						}
					}
				}
			});

			lbl_settingsThemes.setSelected(true);
			pnl_settingsThemeSelection.add(lbl_settingsThemes);

			Win11Label lbl_settingsThemeName = new Win11Label(Theme.themes.get(key).getDisplayName());

			if (Win11LookAndFeel.themeName.replace('_', ' ').equals(key.replace('_', ' '))) {
					lbl_settingsThemeName.setFont(font);
			}

			pnl_settingsThemeSelection.add(lbl_settingsThemeName);
		}

		pnl_rightPanel.add(sp_settingsThemes, BorderLayout.CENTER);

		validate();
		repaint();
	}

	public void loadAccountSettings() {
		pnl_rightPanel.removeAll();

		lbl_settingsAccountInfo = new Win11Label("Accountauswahl - Konto: " + DataConnectionHandler.getInstance().getUsername());
		lbl_settingsAccountInfo.setFont(font);
		pnl_rightPanel.add(lbl_settingsAccountInfo, BorderLayout.NORTH);

		Win11Panel pnl_settingsAccountChange = new Win11Panel();

		GridBagLayout gbl_settingsAccountChange = new GridBagLayout();
		gbl_settingsAccountChange.columnWidths = new int[] {0};
		gbl_settingsAccountChange.rowHeights = new int[] {0};
		gbl_settingsAccountChange.columnWeights = new double[] {1};
		gbl_settingsAccountChange.rowWeights = new double[] {1};
		pnl_settingsAccountChange.setLayout(gbl_settingsAccountChange);
		pnl_rightPanel.add(pnl_settingsAccountChange, BorderLayout.CENTER);

		Win11PriorityButton btn_settingsAccountChange = new Win11PriorityButton("Account wechseln");
		btn_settingsAccountChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BenutzerLoginGUI login = new BenutzerLoginGUI(thisGUI);
				login.setVisible(true);

				setVisible(false);
			}
		});

		GridBagConstraints gbc_btn_settingsAccountChange = new GridBagConstraints();
		gbc_btn_settingsAccountChange.gridx = 0;
		gbc_btn_settingsAccountChange.gridy = 0;
		gbc_btn_settingsAccountChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsAccountChange.insets = new Insets(0, 20, 0, 20);
		pnl_settingsAccountChange.add(btn_settingsAccountChange, gbc_btn_settingsAccountChange);

		validate();
		repaint();
	}

	public void loadDatabaseSettings() {
		pnl_rightPanel.removeAll();

		lbl_settingDatabaseInfo = new Win11Label("Datenbankauswahl " + DataConnectionHandler.getInstance().getServerName() + "/" + DataConnectionHandler.getInstance().getDatabase());
		lbl_settingDatabaseInfo.setFont(font);
		pnl_rightPanel.add(lbl_settingDatabaseInfo, BorderLayout.NORTH);

		Win11Panel pnl_settingsDatabaseChange = new Win11Panel();

		GridBagLayout gbl_settingsDatabaseChange = new GridBagLayout();
		gbl_settingsDatabaseChange.columnWidths = new int[] {0};
		gbl_settingsDatabaseChange.rowHeights = new int[] {0};
		gbl_settingsDatabaseChange.columnWeights = new double[] {1};
		gbl_settingsDatabaseChange.rowWeights = new double[] {1};
		pnl_settingsDatabaseChange.setLayout(gbl_settingsDatabaseChange);
		pnl_rightPanel.add(pnl_settingsDatabaseChange, BorderLayout.CENTER);

		Win11PriorityButton btn_settingsDatabaseChange = new Win11PriorityButton("Datenbank wechseln");



		btn_settingsDatabaseChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DatabaseLoginGUI login = new DatabaseLoginGUI(thisGUI);
				login.setVisible(true);

				setVisible(false);
			}

		});

		GridBagConstraints gbc_btn_settingsDatabaseChange = new GridBagConstraints();
		gbc_btn_settingsDatabaseChange.gridx = 0;
		gbc_btn_settingsDatabaseChange.gridy = 0;
		gbc_btn_settingsDatabaseChange.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_settingsDatabaseChange.insets = new Insets(0, 20, 0, 20);
		pnl_settingsDatabaseChange.add(btn_settingsDatabaseChange, gbc_btn_settingsDatabaseChange);

		validate();
		repaint();
	}

	public void loadAutostartSettings() {
		pnl_rightPanel.removeAll();
		Win11CheckBox cb_settingsAutostartChange = new Win11CheckBox("Autostart wechseln");
		String status = "";
		String username = System.getProperty("user.name");
		File file = new File("C:/Users/" + username + "/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup/tenuschkalender.bat");
		if (file.exists()) {
			status = "Aktiviert";
			cb_settingsAutostartChange.setSelected(true);
		} else {
			status = "Deaktiviert";
			cb_settingsAutostartChange.setSelected(false);
		}

		lbl_settingAutostartInfo = new Win11Label("Autostart: " + status);
		lbl_settingAutostartInfo.setFont(font);
		pnl_rightPanel.add(lbl_settingAutostartInfo, BorderLayout.NORTH);

		Win11Panel pnl_settingsAutostartChange = new Win11Panel();

		GridBagLayout gbl_settingsAutostartChange = new GridBagLayout();
		gbl_settingsAutostartChange.columnWidths = new int[] {0};
		gbl_settingsAutostartChange.rowHeights = new int[] {0};
		gbl_settingsAutostartChange.columnWeights = new double[] {1};
		gbl_settingsAutostartChange.rowWeights = new double[] {1};
		pnl_settingsAutostartChange.setLayout(gbl_settingsAutostartChange);
		pnl_rightPanel.add(pnl_settingsAutostartChange, BorderLayout.CENTER);




		cb_settingsAutostartChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String username = System.getProperty("user.name");
				File file = new File("C:/Users/" + username + "/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup/tenuschkalender.bat");
				if (cb_settingsAutostartChange.isSelected()) {
					try {

						file.createNewFile();
						IOStream stream = new IOStream(file, IOMode.WRITE);
						String path = new File(EinstellungsGUI.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
						stream.addLine("javaw -Xmx512m -jar \"" + path + "\"");
						stream.flush();
						stream.close();
						lbl_settingAutostartInfo.setText("Autostart: Aktiviert");
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (UnkownIOMode e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (WrongIOMode e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					file.delete();
					lbl_settingAutostartInfo.setText("Autostart: Deaktiviert");
				}
			}

		});

		GridBagConstraints gbc_btn_settingsAutostartChange = new GridBagConstraints();
		gbc_btn_settingsAutostartChange.gridx = 0;
		gbc_btn_settingsAutostartChange.gridy = 0;
		pnl_settingsAutostartChange.add(cb_settingsAutostartChange, gbc_btn_settingsAutostartChange);

		validate();
		repaint();
	}

	public Win11Label getLbl_settingDatabaseInfo() {
		return lbl_settingDatabaseInfo;
	}

	public void setLbl_settingDatabaseInfo(Win11Label lbl_settingDatabaseInfo) {
		this.lbl_settingDatabaseInfo = lbl_settingDatabaseInfo;
	}

	public Win11Label getLbl_settingsAccountInfo() {
		return lbl_settingsAccountInfo;
	}

	public void setLbl_settingsAccountInfo(Win11Label lbl_settingsAccountInfo) {
		this.lbl_settingsAccountInfo = lbl_settingsAccountInfo;
	}

}
