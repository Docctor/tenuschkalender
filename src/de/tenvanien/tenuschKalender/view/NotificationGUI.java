package de.tenvanien.tenuschKalender.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import de.tenvanien.tenuschKalender.model.lookAndFeel.Theme;
import de.tenvanien.tenuschKalender.model.lookAndFeel.Win11LookAndFeel;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Button;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Label;
import de.tenvanien.tenuschKalender.model.lookAndFeel.components.Win11Panel;

public class NotificationGUI{

	public NotificationGUI(String caption, String message, int tenuschMessageType) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(new Win11LookAndFeel(Win11LookAndFeel.themeName));
				} catch (Exception e) {
					e.printStackTrace();
				}

				final JDialog dialog = new JDialog((Frame)null);

				JOptionPane optionPane = new JOptionPane();
				optionPane.addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent event) {
						String name = event.getPropertyName();
						if ("value".equals(name)) {
							dialog.dispose();
						}
					}
				});
				int ScreenWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
				int ScreenHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
				int width = 500;
				int height = 200;
				dialog.setBounds(ScreenWidth/2-width/2, ScreenHeight/2-height/2, 0, 0);
				dialog.setPreferredSize(new Dimension(width, height));
				dialog.setUndecorated(true);
				dialog.setLayout(new BorderLayout());
				dialog.add(optionPane);
				dialog.pack();
				dialog.setAlwaysOnTop(true);
				dialog.requestFocus();
				dialog.setShape(new RoundRectangle2D.Double(0, 0, width, height, 16, 16));

				Win11Panel contentPane = new Win11Panel();
				contentPane.setBorder(new EmptyBorder(10,10,10,10));
				dialog.setContentPane(contentPane);
				contentPane.setLayout(new BorderLayout());

				Win11Panel pnl_topPanel = new Win11Panel();
				pnl_topPanel.setLayout(new BorderLayout());
				contentPane.add(pnl_topPanel, BorderLayout.NORTH);

				Win11Label lbl_programmIcon = new Win11Label();
				//Hier müsste eigentlich der tenuschNotifikationType rein, anstatt des TitelBildes
				System.out.println(tenuschMessageType + " Der TenuschMessageType wird noch nicht berücksichtigt");
				lbl_programmIcon.setIcon(Theme.convertImageToImageIcon(Theme.resizeImage(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryIcon(), 15, 15)));
				lbl_programmIcon.setText("Tenusch Kalender");
				pnl_topPanel.add(lbl_programmIcon, BorderLayout.NORTH);

				Win11Panel pnl_topCenterPanel = new Win11Panel();
				pnl_topCenterPanel.setLayout(new BorderLayout());
				pnl_topCenterPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
				pnl_topPanel.add(pnl_topCenterPanel, BorderLayout.CENTER);

				Win11Label lbl_caption = new Win11Label();
				lbl_caption.setFont(new Font(Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getFontName(), Font.BOLD, Theme.themes.get(Win11LookAndFeel.themeName).getPrimaryFont().getSize()));
				lbl_caption.setText("<html><p style=\"width:" + (width-120) +"px\">" + caption + "</p></html>");
				pnl_topCenterPanel.add(lbl_caption, BorderLayout.NORTH);

				Win11Label lbl_message = new Win11Label();
				lbl_message.setText("<html><p style=\"width:" + (width-120) +"px\">" + message + "</p></html>");
				pnl_topCenterPanel.add(lbl_message, BorderLayout.CENTER);

				Win11Button btn_ok = new Win11Button("Schließen");
				contentPane.add(btn_ok, BorderLayout.SOUTH);

				btn_ok.addActionListener(new ActionListener() {

		            @Override
		            public void actionPerformed(ActionEvent e) {
		            	dialog.dispose();
		            }
		        });

				dialog.setVisible(true);
			}
		});


	}

}
